.PHONY: _CoqProject Makefile.coq all clean count_explicit_coercions

all: Makefile.coq
	@make -f Makefile.coq

_CoqProject:
	@echo "-R . Ici" > _CoqProject
	@find theories -name "*.v" >> _CoqProject
	@find examples -name "*.v" >> _CoqProject
	@echo "Regenerating _CoqProject"

Makefile.coq: _CoqProject
	@coq_makefile -f _CoqProject -o Makefile.coq

clean: Makefile.coq
	@find . -name "*~" -delete
	@find . -name "*.aux" -delete
	@make -f Makefile.coq clean
	@rm Makefile.coq _CoqProject Makefile.coq.conf

count_explicit_coercions:
	@find . -name "*.v" | xargs grep -o -E '\%:?[A-Z]' | wc -l

count_Fs:
	@find . -name "*.v" | xargs grep -o -E 'F[0-9]' | wc -l

count_FIXMEs:
	@find . -name "*.v" | xargs grep FIXME | wc -l
