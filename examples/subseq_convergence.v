From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum.
From mathcomp Require Import classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.nat filters.reals.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section Example.

Variable u: ℕ -> ℝ.
Variable l: ℝ.

Hypothesis u_even_cvg: (fun n => u (2 * n)%N) @ ∞ --> l%:E.
Hypothesis u_odd_cvg: (fun n => u (2 * n + 1)%N) @ ∞ --> l%:E.

Lemma u_cvg: u @ ∞ --> l%:E.
Proof.
move=> W Wnb.
move: (u_even_cvg Wnb) => [U [Unb uevenUW]].
move: Unb => [Neven NevenU].
simpl in NevenU. (* F1 *)
move: (u_odd_cvg Wnb) => [V [Vnb uoddVW]].
move: Vnb => [Nodd NoddV].
simpl in NoddV. (* F1 *)
pose N := maxn (2 * Neven) (2 * Nodd + 1).
apply filter_from_sequential_family_exists. (* F1 *)
exists N.
rewrite image_sub /preimage.
move=> n.
simpl. (* F1 *)
move=> n_big.
rewrite -inE. (* F3 *)
case (nat_even_odd_dec n).
- (* n is even *)
  move=> [m n_eq2m].
  have m_geNeven: (Neven <= m)%N.
    rewrite -(@leq_pmul2l 2) => //. (* FIXME: quite ugly *)
    rewrite -n_eq2m.
    apply leq_trans with N => //.
    by apply leq_maxl.
  in_superset uevenUW.
  rewrite inE /mkset //=. (* F3 *)
  exists m ; rewrite ?n_eq2m=> //.
  rewrite -inE. (* F3 *)
  in_superset NevenU.
  by rewrite inE. (* F3 *)
- (* n is odd *)
  move=> [m n_eq2m1].
  have m_geNodd: (Nodd <= m)%N.
    rewrite -(@leq_pmul2l 2) => //=. (* FIXME: quite ugly *)
    rewrite -(leq_add2r 1).
    rewrite -n_eq2m1.
    apply leq_trans with N => //.
    by apply leq_maxr.
  in_superset uoddVW.
  rewrite inE /mkset //=. (* F3 *)
  exists m ; rewrite ?n_eq2m1 => //.
  rewrite -inE. (* F3 *)
  in_superset NoddV.
  by rewrite inE. (* F3 *)
Qed.

End Example.
