From mathcomp Require Import ssreflect ssralg ssrbool ssrnum.
From mathcomp Require Import classical_sets functions constructive_ereal reals mathcomp_extra.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.nat filters.reals.

Import Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Open Scope ring_scope.

Section Example.

Variables u v: ℕ -> ℝ.

Hypothesis u_01: forall n, 0 <= u n <= 1.
Hypothesis v_01: forall n, 0 <= v n <= 1.
Hypothesis uv_cvg1: u \* v @ ∞ --> 1%:E.

Lemma u_cvg1: u @ ∞ --> 1%:E.
Proof.
have u_ge0: forall n, 0 <= u n.
  move=> n.
  move/andP: (u_01 n).
  by apply proj1.
have v_ge0: forall n, 0 <= v n.
  move=> n.
  move/andP: (v_01 n).
  by apply proj1.
have u_le1: forall n, u n <= 1.
  move=> n.
  move/andP: (u_01 n).
  by apply proj2.
have v_le1: forall n, v n <= 1.
  move=> n.
  move/andP: (v_01 n).
  by apply proj2.
have u_within_uv_1: forall n, (u \* v) n <= u n <= 1.
  move=> n.
  apply/andP ; split => //.
  unfold GRing.mul_fun. (* F2 *)
  rewrite -[X in _ <= X]mulr1. (* FIXME: bad *)
  by apply ler_pM => //.
apply limit_real_squeeze with (u \* v) (cst 1) => //.
- by apply limit_real_constant.
- by exists setT.
Qed.

(* and likewise, v @ oo --> 1%:E *)

End Example.
