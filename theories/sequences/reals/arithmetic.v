(* This file is about arithmetic real sequences *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnum order.
From mathcomp Require Import classical_sets functions constructive_ereal reals.

Import Bool Order.Theory Num.Theory GRing.Theory.

From Ici Require Import missing filters.generalities filters.nat filters.reals.
From Ici Require Import sequences.reals.bounded.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: limit of arithmetic sequences
   MC-A: TODO
   mathlib: TODO
 *)

Lemma limit_real_seq_arithmetic:
  forall (a b: ℝ), [/\ a = 0 -> (fun n => a * n%:R + b) @ ∞ --> b%:E,
                       a > 0 -> (fun n => a * n%:R + b) @ ∞ --> +oo
                     & a < 0 -> (fun n => a * n%:R + b) @ ∞ --> -oo].
Proof.
move=> a b.
split => [ -> | | ].
- apply filter_tends_to_asymptotic with (cst b).
  + exists setT; split=> //.
    move=> ?.
    by rewrite mul0r add0r.
  + apply filter_tends_to_constant.
    by apply filter_real_finite_is_pointed.
- move=> a_gt0.
  have ->: +oo = (a%:E * +oo + b%:E)%E by rewrite gt0_muley //.
  apply limit_real_add.
  + by rewrite gt0_muley.
  + apply limit_real_scale. (* FIXME: second introduced goal looks awful *)
    -- rewrite /mule_def //.
       rewrite orb_false_r andb_true_r. (* FIXME: not automatic !? *)
       rewrite eqe.
       by rewrite lt0r_neq0.
    -- by apply limit_real_nat.
  + apply filter_tends_to_constant.
    by apply filter_real_finite_is_pointed.
- move=> a_lt0.
  have ->: -oo = (a%:E * +oo + b%:E)%E by rewrite lt0_muley //.
  apply limit_real_add.
  + by rewrite lt0_muley.
  + apply limit_real_scale. (* FIXME: second introduced goal looks awful *)
    -- rewrite /mule_def //.
       rewrite orb_false_r andb_true_r. (* FIXME: not automatic !? *)
       rewrite eqe.
       by rewrite ltr0_neq0.
    -- by apply limit_real_nat.
  + apply filter_tends_to_constant.
    by apply filter_real_finite_is_pointed.
Qed.
