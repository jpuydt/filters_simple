(* This file is about geometric real sequences *)

From mathcomp Require Import ssreflect ssrbool ssralg ssrnat ssrnum order.
From mathcomp.algebra_tactics Require Import ring.
From mathcomp Require Import boolp classical_sets functions constructive_ereal reals.

Import Order.Theory Num.Theory GRing.Theory.

From Ici Require Import missing filters.generalities filters.nat filters.reals.
From Ici Require Import sequences.reals.bounded sequences.reals.arithmetic.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: limit of geometric sequences
   MC-A: TODO
   mathlib: TODO
 *)

Lemma limit_real_seq_power:
  forall q: ℝ, 0 < q -> [/\ q < 1 -> (fun n => q ^+ n) @ ∞ --> 𝒩[>] 0,
                            q = 1 -> (fun n => q ^+ n) @ ∞ --> 1%:E
                          & q > 1 -> (fun n => q ^+ n) @ ∞ --> +oo].
Proof.
have source: forall q, 1 < q -> (fun n => q ^+ n) @ ∞ --> +oo.
  move=> q.
  rewrite -subr_gt0.
  move=> qN1_gt0.
  apply limit_real_pinfty_by_minoration with (fun n => (q - 1) * n%:R + 1).
  - by apply limit_real_seq_arithmetic.
  - exists setT; split=> //.
    move=> n _.
    elim: n ; first by rewrite mulr0 add0r expr0.
    move=> n IHn.
    calc_on ((q - 1) * (n.+1)%:R + 1).
    calc_le ((q - 1) * (n%:R + q ^+ n) + 1).
      rewrite lerD2r.
      rewrite -natr1.
      rewrite !mulrDr.
      rewrite lerD2l.
      apply ler_pM => //.
      + by apply ltW.
      + apply exprn_ege1.
        by rewrite ltW // -subr_gt0.
    calc_eq ((q - 1) * q ^+ n + (q - 1) * n%:R + 1).
      by ring.
    calc_le ((q - 1) * q ^+ n + q ^+n).
      by rewrite -addrA lerD2l.
    calc_eq (q ^+ n.+1).
      rewrite exprS.
      by ring.
    by [].
move=> q.
split ; last by apply source.
- move=> abs_q_lt1.
  have ->: (fun n => (q ^+ n)) = (fun n => (q^-1 ^+ n) ^-1).
    apply functional_extensionality_dep.
    move=> n.
    by rewrite -exprVn invrK.
  apply limit_real_inv_pinfty.
  apply source.
  by rewrite invf_gt1.
- move ->.
  apply filter_tends_to_asymptotic with (cst 1).
  + exists setT ; split=> //.
    move=> n _.
    by rewrite expr1n.
  + by apply limit_real_constant.
Qed.

















