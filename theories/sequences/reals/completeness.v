(* This file is about completeness of the reals *)

(* the theorem of Bolzano-Weierstrass is for first years ; the rest is
   so it's available to prove other student-admitted results, but not
   for students themselves *)

From mathcomp Require Import ssreflect ssrbool ssralg ssrnat ssrnum order bigop fintype.
From mathcomp Require Import classical_sets constructive_ereal reals functions.

Import Order.Theory Num.Theory GRing.Theory.

From Ici Require Import missing filters.generalities filters.nat filters.reals.
From Ici Require Import sequences.reals.bounded.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


(* RESULT: theorem of Bolzano-Weierstrass *)
Lemma seq_real_Bolzano_Weierstrass
  {u: ℕ -> ℝ} (u_bounded: bounded u setT):
  exists (l: ℝ), exists (phi: ℕ -> ℕ),
    (forall (m n: ℕ), (m < n)%N -> (phi m < phi n)%N)
    /\ (fun n => u (phi n)) @ ∞ --> l%:E.
Proof.
Admitted. (* FIXME: proving it with Coq looks non trivial *)



(* DEFINITION: when is a real sequence said to have the Cauchy property? *)
Definition seq_real_is_Cauchy (u: ℕ -> ℝ) :=
  forall V, V \is_nb 0%:E -> exists U, U \is_nb ∞ /\ (forall m n, m ∈ U -> n ∈ U -> u m - u n ∈ V).



(* RESULT: real sequences satisfying the Cauchy property are bounded *)
Lemma seq_real_Cauchy_is_bounded u:
  seq_real_is_Cauchy u -> bounded u setT.
Proof.
move=> u_Cauchy.
apply seq_real_bounded_asympt.
move: (u_Cauchy (filter_real_item 0%R 0) (filter_real_contains_items 0%R 0))=> [U [Unb HU]]. (* F1 *)
move: (Unb) => [N HN]. (* F1 *)
exists (filter_nat_item N).
split => //.
exists (`|u N| + 1).
move=> n.
rewrite inE //=. (* F1 *)
move=> Nlen.
have ->: u n = u N + (u n - u N) by rewrite addrC -addrA [_ + u N]addrC subrr addr0. (* FIXME *)
apply le_trans with ( `|u N| + `|u n - u N|) ; first by apply ler_normD.
apply lerD => //.
have Un: n ∈ U.
  apply (in_superset HN).
  by rewrite inE. (* F1 *)
have UN: N ∈ U.
  apply (in_superset HN).
  by rewrite inE //=. (* F1 *)
move: (HU n N Un UN).
rewrite inE //=. (* F1 *)
by rewrite expr0 subr0 invr1 mul1r.
Qed.



(* RESULT: real sequences satisfying the Cauchy property actually converge *)
Lemma seq_real_Cauchy_is_convergent u:
  seq_real_is_Cauchy u -> exists l, u @ ∞ --> l%:E.
Proof.
move=> u_Cauchy.
have u_bounded: bounded u setT by apply seq_real_Cauchy_is_bounded.
(* first step: find the would-be limit with a converging subsequence *)
move: (seq_real_Bolzano_Weierstrass u_bounded) => [l [phi [phi_incr uphi_conv]]]. (* F4 *)
exists l.
(* aside: useful lemma *)
have phi_ge_id: forall n, (n <= phi n)%N.
  elim=> [// | n IHn].
  apply leq_ltn_trans with (phi n) => //.
  by apply phi_incr.
(* second step: take the target neighbourhood and cut it by sum *)
move=> W Wnb. (* F1 *)
(* FIXME: the three next lines are awful *)
move: (Wnb).
have ->: l = l + 0 by rewrite addr0.
move=> Wnb_bis.
move: (filter_real_finite_add Wnb_bis) => [Wl [W0 [Wlnb W0nb Wl_plus_W0_in_W]]]. (* F4 *)
(* third step: get in the first sub-neighbourhood through the subsequence *)
move: (uphi_conv Wl Wlnb)=> [V1 [V1nb uphiV1Wl]]. (* F4 *)
(* fourth step: get in the second sub-neighbourhood through the Cauchy property *)
move: (u_Cauchy W0 W0nb)=> [V2 [V2nb uV2diffW0]]. (* F4 *)
(* fifth step: work with a simpler neighbourhood *)
move: (filter_cap ∞ V1 V2 V1nb V2nb) => [N HN].
apply filter_from_sequential_family_exists.
exists N.
rewrite image_sub /preimage.
move=> n //= Nlen.
rewrite -inE. (* F3 *)
(* final: we cut expression in terms and prove they are in the various neighbourhoods *)
have ->: u n = u (phi n) + (u n - u (phi n)) by rewrite -addrCA subrr addr0. (* FIXME *)
apply Wl_plus_W0_in_W.
- apply (in_superset uphiV1Wl).
  rewrite imagePin //.
  have V1capV2subV1: V1 ∩ V2 ⊂ V1 by apply subIsetl.
  apply (in_superset V1capV2subV1).
  apply (in_superset HN).
  by rewrite inE.
- apply uV2diffW0.
  + have V1capV2subV2: V1 ∩ V2 ⊂ V2 by apply subIsetr.
    apply (in_superset V1capV2subV2).
    apply (in_superset HN).
    by rewrite inE.
  + have V1capV2subV2: V1 ∩ V2 ⊂ V2 by apply subIsetr.
    apply (in_superset V1capV2subV2).
    apply (in_superset HN).
    rewrite inE //=.
    by apply leq_trans with n.
Qed.
