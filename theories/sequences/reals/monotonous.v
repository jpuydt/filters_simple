(* This file is about monotonous real sequences *)

From mathcomp Require Import ssrbool ssreflect ssralg ssrnat ssrnum order.
From mathcomp.algebra_tactics Require Import ring.
From mathcomp Require Import boolp classical_sets functions constructive_ereal mathcomp_extra reals functions.

Import Num.Theory GRing.Theory Order.Theory.

From Ici Require Import missing filters.generalities filters.nat filters.reals.
From Ici Require Import sequences.reals.bounded.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* FIXME: the lack of a sup: set ℝ -> \bar ℝ is annoying
   FIXME  as we would like any monotonous sequence to have
   FIXME  a limit -- and a finite limite iff bounded
 *)



(* RESULT: a nondecreasing bounded real sequence has a limit
   MC-A: nondecreasing_cvg in sequences.v
   mathlib: TODO
 *)

Lemma limit_real_seq_nondecreasing_bounded:
  forall (u: ℕ -> ℝ),
    (forall n, u n <= u n.+1)
 -> bounded u setT
 -> u @ ∞ --> (sup (range u))%:E.
Proof.
move=> u u_nondecr u_bound.
have {u_bound} has_u_sup: has_sup (range u).
  split ; first by exists (u 0%N).
  by apply bounded_has_ubound.
move=> W Wnbsup.
have: exists w, w ∈ (range u ∩ W) /\ forall a, a ∈ range u -> w <= a -> a ∈ W.
  by apply filter_real_upper_bound.

move=> [y [yW W_abovew]].
move: yW. rewrite in_setI => /andP [yuN Wy]. (* FIXME: annoying *)
(* FIXME: too many lines to get from an image element to its representation *)
move: yuN.
rewrite inE. (* F3 *)
move => [N _]. (* F4 uh... *)
move=> yuN.

rewrite -yuN in W_abovew.
rewrite -yuN in Wy.

apply filter_from_sequential_family_exists. (* F1 *)
exists N.
rewrite image_sub /preimage.
simpl. (* F1 *)
move=> n.
rewrite/mkset => N_le_n. (* F3 *)
rewrite -inE. (* F3 *)
apply W_abovew.
- apply imagePin.
  by rewrite inE. (* F3 *)
- by apply seq_nondecreasing_iff_onestep.
Qed.



(* RESULT: a nonincreasing real sequence has a limit
   MC-A: nonincreasing_cvg
   mathlib: TODO
*)

Lemma limit_real_seq_nonincreasing_bounded:
  forall (u: ℕ -> ℝ),
    (forall n, u n.+1 <= u n)
 -> bounded u setT
 -> u @ ∞ --> (inf (range u))%:E.
Search "inf" "N".
Search "inf" "opp".
Admitted. (* FIXME: I don't see results mixing opp with has_lbound, inf, sup etc *)



(* RESULT: adjacent sequences
   MC-A: TODO
   mathlib: TODO
*)

Lemma limit_real_seq_adjacent:
  forall (u v: ℕ -> ℝ),
  (forall n, u n.+1 <= u n) -> (forall n, v n <= v n.+1) -> (v \- u) @ ∞ --> 0%:E
  -> exists l, u @ ∞ --> l%:E /\ v @ ∞ --> l%:E.
Proof.
move=> u v u_nonincr v_nondecr vNu0.

have vNu_nondecr: forall n, (v \- u) n <= (v \- u) n.+1.
  move=> n.
  rewrite lerBDl addrA lerBDr.
  rewrite addrC.
  by apply lerD.

have vNu_bounded: bounded (v \- u) setT.
  apply seq_real_convergent_is_bounded.
  by exists 0.

have vNusup0: sup (range (v \- u)) = 0 :> ℝ.
  apply/EFin_inj.
  (* FIXME: too much to say *)
  apply filter_tends_to_unicity with (limit := filter_real)
                                     (f := v \- u) (F := ∞) => //.
  - by apply filter_nat_nontrivial.
  - by apply limit_real_seq_nondecreasing_bounded.

have v_le_u: forall n, v n <= u n.
  move=> n.
  rewrite -subr_le0.
  have ->: v n - u n = (v \- u) n by rewrite /GRing.sub_fun. (* F2 *)
  rewrite -vNusup0.
  apply sup_ub => //.
  by apply bounded_has_ubound.

have u_le_u0: forall n, u n <= u 0%N.
  elim=> [ // | n ].
  by apply le_trans.

have v_le_u0: forall n, v n <= u 0%N.
  elim=> [ // | n ].
  move=> _.
  by apply le_trans with (u n.+1).

have v0_le_v: forall n, v 0%N <= v n.
  elim=> [ // | n ].
  move=> v0_le_vn.
  by apply le_trans with (v n).

have v_double_inequality: forall n, v 0%N <= v n <= u 0%N.
  move=> n.
  by apply/andP ; split.

have v_convergent: v @ ∞ --> (sup (range v))%:E.
  apply limit_real_seq_nondecreasing_bounded => //.
  apply seq_real_bounded_double_inequality.
  exists (v 0%N).
  exists (u 0%N).
  move=> n _.
  by apply v_double_inequality.

exists (sup (range v)) ; split=> //.

have ->: u = v \+ (\- (v \- u)).
  apply functional_extensionality_dep.
  move=> x.
  unfold GRing.add_fun. (* F2 *)
  unfold GRing.opp_fun. (* F2 *)
  unfold GRing.sub_fun. (* F2 *)
  by ring.

rewrite -[(sup _)%:E]adde0.
apply limit_real_add => //.
rewrite -oppe0.
by apply limit_real_opp.
Qed.
