(* This file is about bounded real sequences *)

From mathcomp Require Import ssreflect ssrbool ssralg ssrnat ssrnum order bigop fintype.
From mathcomp Require Import classical_sets constructive_ereal reals.

Import Order.Theory Num.Theory GRing.Theory.

From Ici Require Import missing filters.generalities filters.nat filters.reals.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(* RESULT: a real sequence is bounded if and only if it is ultimately bounded
   MC-A: TODO
   mathlib: TODO
 *)

Lemma seq_real_bounded_asympt
  (u: ℕ -> ℝ):
  [locally ∞, bounded u] -> bounded u setT.
Proof.
move=> [W [[N BNW]]]. (* F4 [locally ...] and go to a filter basis item *)
move=> [M HM]. (* F4 bounded *)
exists (Num.max M (\big[Num.max/0]_(i < N.+1) `|u i|)).
move=> n _.
case/orP: (leq_total N n).
- move=> N_le_n.
  apply le_trans with M ; last first.
    rewrite le_max.
    by apply/orP ; left.
  apply HM.
  in_superset BNW.
  by rewrite inE. (* F3 *)
- move=> n_le_N.
  rewrite le_max.
  apply/orP ; right.
  rewrite (bigmaxD1 (inord n)) //. (* FIXME: le_bigmax doesn't work! *)
  rewrite le_max.
  apply/orP ; left.
  by rewrite inordK.
Qed.


(* RESULT: a real sequence is bounded if and only if it's controlled below and above *)

Lemma seq_real_bounded_double_inequality
 (u: ℕ -> ℝ) (M: set ℕ):
 (exists a b, forall n, n ∈ M -> a <= u n <= b) -> bounded u M.
Proof.
move=> [a [b Hab]].
exists (`|a|+`|b|). (* FIXME: ugly, but simpler than with max *)
move=> n nM.
rewrite ler_norml.
apply/andP ; split.
- rewrite opprD lerBDr.
  apply ler_wpDr => //.
  apply le_trans with a.
  + rewrite -lerN2 opprK -normrN.
    by apply ler_norm.
  + move/andP: (Hab _ nM). (* FIXME: apply won't work *)
    by apply proj1. (* FIXME: inelegant *)
- apply le_trans with b.
  + move/andP: (Hab _ nM). (* FIXME: apply won't work *)
    by apply proj2. (* FIXME: inelegant *)
  + apply ler_wpDl => //.
    by apply ler_norm.
Qed.



(* RESULT: a convergent real sequence is bounded *)

Lemma seq_real_convergent_is_bounded
 (u: ℕ -> ℝ):
  (exists l, u @ ∞ --> l%:E) -> bounded u setT.
Proof.
move=> [l u_tendsto_l].
apply seq_real_bounded_asympt.
have: [locally ∞, fun V => [set u x | x in V] ⊂ filter_real_item l%:E 0].
  by apply u_tendsto_l.
apply filter_prop_locally_imp.
simpl. (* F1 *)
rewrite expr0 divr1.
move=> M HM.
apply seq_real_bounded_double_inequality.
exists (l - 1) ; exists (l + 1).
move=> n nM.
rewrite -ler_distl.
apply HM.
exists n => //.
by rewrite -inE. (* F3 *)
Qed.
