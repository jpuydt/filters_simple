(* This file contains things I find missing in Coq/MC/MC-A *)

From mathcomp Require Import ssreflect eqtype ssrbool order.
From mathcomp Require Import reals.

Import Order.TotalTheory.

From Ici Require Import missing.notations missing.calc_chains missing.exp2n missing.order missing.inequalities missing.nat missing.set missing.bounded.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Require Export missing.notations missing.calc_chains missing.exp2n missing.order missing.inequalities missing.nat missing.set missing.bounded.


(* below are things too small to deserve a specific file *)


Lemma real_trisection (x y: ℝ): [\/ x = y, x < y | y < x].
Proof.
move: (eqVneq x y) => [ -> | xneqy].
  by apply Or31.
case/orP: (lt_total xneqy) => H.
  by apply Or32.
by apply Or33.
Qed.
