(* This file contains the basic definitions and theorems on filter and limits *)

From mathcomp Require Import ssreflect eqtype ssrbool ssrfun.
From mathcomp Require Import classical_sets boolp functions.

From Ici Require Import missing.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: what is a filter?
   MC-A: Class Filter in topology.v
   mathlib: structure filter in order/filter/basic.lean
 *)

Record Filter (E: Type) := mkFilter {
  filter_has: (set E) -> Prop;

  filter_fullset: filter_has setT;
  filter_nondecr: forall A B, A ⊂ B -> filter_has A -> filter_has B;
  filter_cap: forall A B, filter_has A -> filter_has B -> filter_has (A ∩ B);
}.
Arguments filter_has {E}.
Arguments filter_fullset {E}.
Arguments filter_nondecr {E}.
Arguments filter_cap {E}.

Reserved Notation "V '\is_nb' F" (at level 70).
Notation "V '\is_nb' F" := (filter_has F V).

#[global] Hint Resolve filter_fullset : core.
#[global] Hint Extern 0 ((_ ∩ _) \is_nb _) => apply filter_cap ; done : core.



(* DEFINITION: non trivial filter
   Indeed, if a filter contains the empty set, then it contains all subsets...
 *)

Definition filter_nontrivial
  {X: Type}
  (F: Filter X) :=
  forall U, U \is_nb F -> U !=set0.



(* DEFINITION: limit of a function
   MC-A: cvg_to in topology.v
   mathlib: tendsto in order/filter/basic.lean
 *)

Definition filter_tends_to
  {X Y: Type}
  (f: X -> Y)
  (F: Filter X)
  (G: Filter Y) :=
  forall W, W \is_nb G -> exists V, V \is_nb F /\ f @` V ⊂ W.


Reserved Notation "f @ F --> G" (at level 70).
Notation "f @ F --> G" := (filter_tends_to f F G).



(* RESULT: theorem of composition of limits
   MC-A: cvg_comp in topology.v
   mathlib: tendsto.comp in order/filter/basic.lean
 *)

Theorem filter_tends_to_composition
  {X Y Z: Type}
  (F: Filter X) (G: Filter Y) (H: Filter Z)
  (f: X -> Y) (g: Y -> Z):

  f @ F --> G -> g @ G --> H -> g \o f @ F --> H.
Proof.
move=> tendsf tendsg.
move=> W Wnb.
have: exists V, V \is_nb G /\ [set g x | x in V] ⊂ W.
  by apply tendsg.
move=> [V [Vnb compg]].
have: exists U, U \is_nb F /\ [set f x | x in U] ⊂ V.
  by apply tendsf.
move=> [U [Unb compf]].
exists U ; split => //.
apply subset_trans with [set g x | x in V] => //.
rewrite -image_comp.
by apply image_subset.
Qed.



(* RESULT: constant functions have limits
   MC-A: cvg_cst in topology.v
   mathlib: tendsto_const_nhds in topology/basic.lean
 *)

Lemma filter_tends_to_constant
  (X: Type) (F: Filter X)
  (Y: Type) (G: Filter Y) (y: Y):
  (forall V, V \is_nb G -> y ∈ V) -> (cst y) @ F --> G.
Proof.
move=> Gy.
move=> W Wnb. (* F4 filter_tendsto *)
exists setT ; split => //.
apply subset_trans with [set y].
- by apply image_cst.
- rewrite set1_subset.
  by apply Gy.
Qed.



(* DEFINITION: separated family of filters
   It's the natural notion to consider to get unicity of limits results.
   If you add too many possible limits on a set, you lose unicity!
   For example the sequence (1/2^n)_n converges to both 0 and 0+...
 *)

Definition filter_family_is_separated
  {E: Type} {L: eqType} (limit: L -> Filter E) :=
  forall (l1 l2: L),
    l1 != l2 -> exists (V1 V2: set E), [/\ V1 \is_nb limit l1,
                                           V2 \is_nb limit l2
                                         & V1 ∩ V2 =set0].



(* RESULT: unicity of limit
   MC-A: cvg_unique in topology.v
   mathlib: tendsto.not_tendsto in order/filter/basic.lean?
 *)

Theorem filter_tends_to_unicity
  {X: Type} (F: Filter X)
  {Y: Type} {L: eqType} (limit: L -> Filter Y)
  (f: X -> Y):
  filter_family_is_separated limit
  -> filter_nontrivial F
  -> forall (l1 l2: L), f @ F --> limit l1 -> f @ F --> limit l2 -> l1 = l2.
Proof.
move=> Lseparated Fnontrivial l1 l2 ftendsto_l1 ftendsto_l2.
case: (eqVneq l1 l2) => // l1neql2.
have: exists V1 V2, [/\ V1 \is_nb limit l1,
                        V2 \is_nb limit l2
                      & (V1 ∩ V2) = set0].
  by apply Lseparated.
move=> [V1 [V2 [V1nb V2nb V1IV2empty]]].
have: exists U1, U1 \is_nb F /\ [set f x | x in U1] ⊂ V1.
  by apply ftendsto_l1.
move=> [U1 [U1nb fU1V1]].
have: exists U2, U2 \is_nb F /\ [set f x | x in U2] ⊂ V2.
  by apply ftendsto_l2.
move=> [U2 [U2nb fU2V2]].
have: U1 ∩ U2 !=set0.
  by apply Fnontrivial.
move=> [x].
rewrite -inE. (* F3 *)
rewrite in_setI => /andP [V1x V2x]. (* FIXME: too long *)
have: f x ∈ (V1 ∩ V2).
  rewrite in_setI.
  apply/andP ; split.
  - in_superset fU1V1.
    by apply imagePin.
  - in_superset fU2V2.
    by apply imagePin.
by rewrite in_setE V1IV2empty. (* F3 *)
Qed.
