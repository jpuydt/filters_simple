(* this file contains what is needed to build the most basic filters - first year material *)


From mathcomp Require Import ssreflect ssrbool ssrnat.
From mathcomp Require Import classical_sets.

From Ici Require Import missing filters.basics.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(* DEFINITION: generating a filter using a sequence of neighbourhoods *)

Record FilterSequentialFamily (E: Type) := mkFilterSequentialFamily {
  seq_item: ℕ -> set E;
  seq_item_nonincr: forall m n, m <= n -> seq_item n ⊂ seq_item m;
}.

Section FilterSequentialFamily.

  Variable E: Type.
  Variable G: FilterSequentialFamily E.

  Definition filter_sequential_family: set (set E) := fun A => exists i, seq_item G i ⊂ A.

  Lemma filter_sequential_family_fullset: filter_sequential_family setT.
  Proof.
    by exists 0.
  Qed.

  Lemma filter_sequential_family_nondecr:
    forall A B, A ⊂ B -> filter_sequential_family A -> filter_sequential_family B.
  Proof.
    move=> A B AsubB [m Am].
    exists m.
    by apply (subset_trans Am).
  Qed.

  Lemma filter_sequential_family_cap:
    forall A B, filter_sequential_family A -> filter_sequential_family B
                -> filter_sequential_family (A ∩ B).
  Proof.
    move=> A B [m Am] [n Bn].
    exists (m + n).
    rewrite subsetI ; split.
    - apply: (subset_trans _ Am).
      apply seq_item_nonincr.
      by apply leq_addr.
    - apply: (subset_trans _ Bn).
      apply seq_item_nonincr.
      by apply leq_addl.
  Qed.

  Definition filter_from_sequential_family :=
    mkFilter
      filter_sequential_family_fullset
      filter_sequential_family_nondecr
      filter_sequential_family_cap.

End FilterSequentialFamily.



(* RESULT: neighbourhoods generating a filter are part of it *)

Lemma filter_sequential_family_contains_items
  {E: Type} (G: FilterSequentialFamily E):
  forall n, seq_item G n \is_nb filter_from_sequential_family G.
Proof.
  move=> n.
  by exists n.
Qed.

#[global] Hint Resolve filter_sequential_family_contains_items: core.



(* RESULT: if the family doesn't contain the empty set, the filter is non trivial *)

Lemma filter_sequential_nontrivial
  {E: Type} {G: FilterSequentialFamily E}:
  (forall n, seq_item G n !=set0) -> filter_nontrivial (filter_from_sequential_family G).
Proof.
  move=> Gnonempty.
  move=> U [N GNU].
  apply (subset_nonempty GNU).
  by apply Gnonempty.
Qed.



(* RESULT: limit of a function on a sequential filter *)

Lemma filter_tendsto_on_sequential
  {X Y: Type} {f: X -> Y} {B: FilterSequentialFamily X} {G: Filter Y}:
  (forall W, W \is_nb G -> exists n, f @` (seq_item B n) ⊂ W)
  <-> f @ (filter_from_sequential_family B) --> G.
Proof.
  split.
  - move=> H_for_generator.
    move=> W Wnb. (* F4 filter_tends_to *)
    have: exists n, [set f x | x in seq_item B n] ⊂ W by apply H_for_generator.
    move=> [n fBnW].
    exists (seq_item B n).
    split.
    + by exists n.
    + by [].
  - move=> ftendsto W Wnb.
    move: (ftendsto W Wnb) => [V [Vnb fVW]].
    move: Vnb => [n Hn]. (* FIXME: looks awful *)
    exists n.
    apply subset_trans with [set f x | x in V] => //.
    by apply image_subset.
Qed.



(* RESULT: limit of a function to a sequential filter *)

Lemma filter_tendsto_to_sequential
  {X Y: Type} {f: X -> Y} {F: Filter X} {B: FilterSequentialFamily Y}:
  (forall n, exists W, W \is_nb F /\ f @` W ⊂ seq_item B n)
  <-> f @ F --> (filter_from_sequential_family B).
Proof.
  split.
  - move=> tendsto_for_generator.
    move=> W Wnb. (* F4 filter_tends_to *)
    move: Wnb => [n eBnW].
    simpl in eBnW. (* F1 *)
    move: (tendsto_for_generator n) => [V [Vnb fVeBn]].
    exists V.
    split.
    + by [].
    + by apply subset_trans with (seq_item B n).
  - move=> ftendsto.
    move=> n.
    have eBn_nb: seq_item B n \is_nb filter_from_sequential_family B by [].
    move: (ftendsto (seq_item B n) eBn_nb) => [V [Vnb fVeBn]].
    by exists V.
Qed.



(* RESULT: limit of function when both filters are sequential *)

Lemma filter_tendsto_sequential
  {X Y: Type} {f: X -> Y}
  (G1: FilterSequentialFamily X)
  (G2: FilterSequentialFamily Y):
  (forall n, exists m, f @` (seq_item G1 m) ⊂ (seq_item G2 n))
  <-> f @ (filter_from_sequential_family G1)
        --> (filter_from_sequential_family G2).
Proof.
split.
- move=> tendsto_item.
  rewrite -filter_tendsto_on_sequential.
  move=> W.
  move=> [n Hn]. (* FIXME: ugly *)
  move: (tendsto_item n) => [m fG1mG2n].
  exists m.
  by apply subset_trans with (seq_item G2 n).
- move=> tendsto_general.
  move=> n.
  have: seq_item G2 n \is_nb filter_from_sequential_family G2 by [].
  move=> eG2nnb.
  move: (tendsto_general (seq_item G2 n) eG2nnb) => [V [Vnb fVeG2]].
  move: Vnb => [m Hm].
  exists m.
  apply subset_trans with [set f x | x in V] => //.
  by apply image_subset.
Qed.
