(* This file is about product and real filters *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum archimedean order eqtype.
From mathcomp Require Import boolp classical_sets constructive_ereal mathcomp_extra reals.
From mathcomp.algebra_tactics Require Import ring.

From Ici Require Import missing filters.generalities filters.reals.definitions.
From Ici Require Import filters.reals.operations.shift filters.reals.operations.scale.
From Ici Require Import filters.reals.operations.opposite filters.reals.operations.addition.

Import Bool Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: product and neighbourhoods *)

Lemma filter_real_mul:
  forall (a b: \bar ℝ),
  a *? b
  -> forall W, W \is_nb (a * b)%E
  -> exists U V, [/\ U \is_nb a,
                     V \is_nb b
                   & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
Proof.
(* first step: prove the lemma in particular cases *)
have zero_zero:
  forall W, W \is_nb 0%E ->
            exists U V, [/\ U \is_nb 0%E, V \is_nb 0%E
                                          & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
  apply filter_from_sequential_family_local_prop. (* F1 *)
  move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [B [Anb Bnb ABU]]].
  exists A; exists B.
  split => //.
  move=> u v Au Bv.
  - in_superset UsubV.
    by apply ABU.
  - move=> n.
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists n ; exists 0%N.
    move=> u v.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite !subr0 expr0 invr1 mulr1.
    move=> Hu Hv.
    rewrite -[1 / 2 ^+ n]mulr1 normrM.
    by apply ler_pM ; rewrite /normr_ge0 //.

have zero_pos:
  forall b: ℝ, 0 < b
  -> forall W, W \is_nb 0%E
               -> exists U V, [/\ U \is_nb 0%E, V \is_nb b%:E
                                                & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
  move=> b b_pos.
  apply filter_from_sequential_family_local_prop. (* F1 *)
  - move=> U V Unb Vnb UsubV. (* F4 filter_prop_local *)
    move=> [A [B [Anb Bnb ABU]]].
    exists A; exists B.
    split=> //.
    move=> u v Au Bv.
    in_superset UsubV.
    by apply ABU.
  - move=> n.
    pose q := Num.bound `|b|.
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists (n + q.+1)%N ; exists 0%N.
    move=> u v.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite !subr0 expr0 invr1 mulr1.
    move=> Hu Hv.
    rewrite normrM.
    calc_on (`|u| * `|v|).
    calc_le (`|u| * (`|v-b| + `|b|)).
      apply ler_pM => //.
      have ->: `|v| = `|v - 0| by rewrite subr0.
      have ->: `|b| = `|b - 0| by rewrite subr0.
      by apply ler_distD.
    calc_le (`|u| * (1 + q%:R)).
      apply ler_pM ; rewrite ?addr_ge0 //.
      apply lerD => //.
      by rewrite ltW // archi_boundP.
    calc_le (`|u| * 2 ^+ (q.+1)).
      rewrite nat1r.
      apply ler_pM => //.
      rewrite -natrX ler_nat.
      apply ltn_trans with (q.+1) => //.
      by apply n_lt_exp2n.
    calc_le ((1 / 2 ^+ (n + q.+1)) * (2 ^+ q.+1): ℝ).
      apply ler_pM => //.
      by rewrite exprn_ge0.
    calc_eq (1 / 2 ^+ n : ℝ).
      rewrite exprD invfM => //.
      rewrite mul1r mulrAC -mulrA.
      rewrite divff ; first by rewrite div1r mulr1.
      by rewrite lt0r_neq0 // exprn_gt0.
    by [].

have pos_pos:
  forall (a b: ℝ), 0 < a -> 0 < b
  -> forall W, W \is_nb (a * b)%:E
               -> exists U V, [/\ U \is_nb a%:E, V \is_nb b%:E
                                                 & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
  move=> a b a_pos b_pos.
  apply filter_from_sequential_family_local_prop. (* F1 look at the awful goal! *)
  - move=> U V Unb Vnb UsubV.
    move=> [A [B [Aa Bb ABU]]].
    exists A.
    exists B; split=> //.
    move=> u v Au Bv.
    in_superset UsubV.
    by apply ABU.
  - move=> n.
    (* this equality explains the neighbourhood calculations *)
    have plan: forall u v, u * v - a * b = (u - a) * (v - b) + (a * (v - b) + b * (u - a)).
      move=> u v.
      by ring.
    (* ok, so we're near a * b, so we'll shift it as X near 0 *)
    have: exists X, X \is_nb 0%E /\ forall u, u ∈ X -> u + a * b ∈ filter_real_item (a * b)%:E n.
      apply filter_real_shift.
      by rewrite add0e.
    move=> [X [Xnb Xab_n]].

    (* now cut X near 0 by sum as AB + CD, both near 0 *)
    have: exists AB CD, [/\ AB \is_nb 0%E, CD \is_nb 0%E
                                           & forall ab cd, ab ∈ AB -> cd ∈ CD -> ab+cd ∈ X].
      apply filter_real_add => //.
      by rewrite add0e.
    move=> [AB [CD [ABnb CDnb ABCDX]]].

    (* ok, AB will be the product of two neighbourhoods of 0 *)
    have: exists A B, [/\ A \is_nb 0%E, B \is_nb 0%E
                                        & forall a b, a ∈ A -> b ∈ B -> a * b ∈ AB].
      by apply zero_zero.
    move=> [A [B [Anb Bnb ABAB]]].
    (* now cut CD into C + D *)
    have: exists C D, [/\ C \is_nb 0%E, D \is_nb 0%E
                                      & forall c d, c ∈ C -> d ∈ D -> c + d ∈ CD].
      apply filter_real_add => //.
      by rewrite add0e.
    move=> [C [D [Cnb Dnb CDCD]]].

    (* now C needs scaling by a *)
    have: exists E, E \is_nb 0%E /\ forall u, u ∈ E -> a * u ∈ C.
      apply filter_real_scale.
      + by rewrite mule_def_fin.
      + by rewrite mule0.
    move=> [E [Enb aEC]].

    (* and D needs scaling by b *)
    have: exists F, F \is_nb 0%E /\ forall v, v ∈ F -> b * v ∈ D.
      apply filter_real_scale.
      + by rewrite mule_def_fin.
      + by rewrite mule0.
    move=> [F [Fnb bFD]].

    (* now we have good neighbourhoods of 0, turn them into two good neighbourhoods of 0 *)
    pose Ushift := A ∩ F.
    have Ushift0: Ushift \is_nb 0%:E by rewrite /Ushift.
    pose Vshift := B ∩ E.
    have Vshift0: Vshift \is_nb 0%:E by rewrite /Vshift.

    (* define our two good neighbourhoods of a and b now *)
    have: exists U, U \is_nb a%:E /\ forall u, u ∈ U -> u - a ∈ Ushift.
      apply filter_real_shift.
      by rewrite -EFinD subrr.
    move=> [U [Unb UNaUshift]].
    have: exists V, V \is_nb b%:E /\ forall v, v ∈ V -> v - b ∈ Vshift.
      apply filter_real_shift.
      by rewrite -EFinD subrr.
    move=> [V [Vnb VNbVshift]].

    (* final stages: piece it together *)
    exists U.
    exists V.
    split=> //.
    move=> u v Uu Vv.
    have uNaUshift: u - a ∈ Ushift.
      by apply UNaUshift.
    have vNbVshift: v - b ∈ Vshift.
      by apply VNbVshift.
    have ->: u * v = u * v - a * b + a * b by ring.
    apply Xab_n.
    rewrite plan.
    apply ABCDX.
    + apply ABAB.
      -- have UshiftsubA: Ushift ⊂ A by rewrite /Ushift.
         by in_superset UshiftsubA.
      -- have VshiftsubB: Vshift ⊂ B by rewrite /Vshift.
        by in_superset VshiftsubB.
    + apply CDCD.
      -- apply aEC.
         have VshiftsubE: Vshift ⊂ E by rewrite /Vshift.
         by in_superset VshiftsubE.
      -- apply bFD.
         have UshiftsubF: Ushift ⊂ F by rewrite /Ushift.
         by in_superset UshiftsubF.

have pos_pinfty:
  forall a: ℝ, 0 < a
  -> forall W, W \is_nb +oo
               -> exists U V, [/\ U \is_nb a%:E, V \is_nb +oo
                                                 & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
  move=> a a_pos.
  apply filter_from_sequential_family_local_prop. (* F1 *)
  - move=> U V Unb Vnb UsubV. (* F4 filter_prop_local *)
    move=> [A [B [Anb Bnb ABU]]].
    exists A.
    exists B.
    split=> //.
    move=> u v Au Vb.
    in_superset UsubV.
    by apply ABU.
  - move=> n.
    pose q := Num.bound (1/a).
    have aN12q: 0 < a - 1 / 2^+ q.
      have ->: 0 = 1 / 2 ^+ q - 1 / 2 ^+ q :> ℝ by rewrite subrr.
      apply ltr_leD => //.
      rewrite -my_ltr_pV2 ; first last.
      + by rewrite inE div1r invr_gt0 exprn_gt0 // ltr0n.
      + by rewrite inE.
      rewrite div1r invrK.
      rewrite -div1r.
      apply lt_trans with q%:R ; last by rewrite -natrX ltr_nat n_lt_exp2n.
      rewrite archi_boundP // ltW //.
      by rewrite div1r invr_gt0.
    pose r := Num.bound (1/(a - 1 / 2 ^+ q)).
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists q ; exists (n * r)%N.
    move=> u v.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> HuNa.
    have u_ge: a - 1 / 2 ^+ q <= u by apply ler_distlCBl.
    move=> v_ge.
    apply le_trans with ((a - 1 / 2 ^+ q) * (n * r)%:R).
    + have -> : (a - 1 / 2 ^+ q) * (n * r)%:R = n%:R * (r%:R * (a - 1 / 2 ^+ q)) by ring.
      rewrite -[leLHS]mulr1.
      apply ler_pM => //.
      rewrite -ler_pdivrMr //.
      rewrite ltW // archi_boundP // ltW //.
      by rewrite mulrC mulr1 invr_gt0. (* FIXME: ugly *)
    + apply ler_pM => //.
      by rewrite ltW.

have pinfty_pinfty:
  forall W, W \is_nb +oo
            -> exists U V, [/\ U \is_nb +oo, V \is_nb +oo
                                             & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
  apply filter_from_sequential_family_local_prop. (* F1 *)
  - move=> U V Unb Vnb UsubV. (* F4 filter_prop_local *)
    move=> [A [B [Anb Bnb ABU]]].
    exists A.
    exists B.
    split=> //.
    move=> u v Au Bv.
    in_superset UsubV.
    by apply ABU.
  - move=> n.
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists 1%N ; exists n.
    move=> u v.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move => uge vge.
    rewrite -[n%:R]mul1r.
    by apply ler_pM.

(* second step: treat all real cases using the preceding ones *)

have real_real:
  forall (a b: ℝ), forall W, W \is_nb (a * b)%:E
    -> exists U V, [/\ U \is_nb a%:E, V \is_nb b%:E
                                      & forall u v, u ∈ U -> v ∈ V -> u * v ∈ W].
  move=> a b.
  apply filter_from_sequential_family_local_prop. (* F1 *)
  - move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
    move=> [A [B [Anb Bnb ABU]]].
    exists A.
    exists B.
    split=> //.
    move=> u v Au Bv.
    in_superset UsubV.
    by apply ABU.
  - move=> n.
    case: (real_trisection a 0).
    + (* a = 0 *)
      move ->.
      rewrite mul0r.
      case: (real_trisection b 0).
      -- (* b = 0 *)
        move ->. (* FIXME: I don't like this *)
        rewrite /seq_item. (* F1 *)
        apply zero_zero.
        by apply filter_real_contains_items. (* F1 hence the Hint Resolve doesn't work! *)
      -- (* b < 0 *)
        rewrite -oppr_gt0 => Nb_pos.
        have: exists A B, [/\ A \is_nb 0%E, B \is_nb (-b)%:E
                            & forall a b, a ∈ A -> b ∈ B -> a * b ∈ filter_real_item 0 n].
          by apply zero_pos.
        move=> [A [B [Anb Bnb AB0n]]].
      have: exists U, U \is_nb 0%E /\ forall u, u ∈ U -> -u ∈ A.
        rewrite -[0%E]oppe0.
        by apply filter_real_opp.
      move=> [U [Unb oppUA]].
      have: exists V, V \is_nb b%:E /\ forall v, v ∈ V -> -v ∈ B.
        rewrite -[b%:E]oppeK.
        by apply filter_real_opp.
      move=> [V [Vnb oppVB]].
      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -[u * v]mulrNN.
      apply AB0n.
      ++  by apply oppUA.
      ++ by apply oppVB.
    -- (* 0 < b *)
      move=> b_pos.
      apply zero_pos => //.
      by apply filter_real_contains_items. (* F1 hence the Hint Resolve doesn't work above *)
  - (* a < 0 *)
    rewrite -oppr_gt0.
    move=> Na_pos.
    case: (real_trisection b 0).
    + (* b = 0 *)
      move=> ->.
      rewrite mulr0.
      have: exists A B, [/\ A \is_nb 0%E, B \is_nb (-a)%:E
                                              & forall x y, x ∈ A -> y ∈ B
                                                            -> x * y ∈ filter_real_item 0 n].
        by apply zero_pos.
      move=> [B [A [Bnb Anb BAW]]].

      have: exists U, U \is_nb a%:E /\ forall u, u ∈ U -> -u ∈ A.
        rewrite -[a%:E]oppeK.
        by apply filter_real_opp.
      move=> [U [Unb oppUA]].

      have: exists V, V \is_nb 0%E /\ forall v, v ∈ V -> -v ∈ B.
        rewrite -[0%E]oppe0.
        by apply filter_real_opp.
      move=> [V [Vnb oppVB]].

      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -[u * v]mulrNN mulrC.
      apply BAW => //.
      ++ by apply oppVB.
      ++ by apply oppUA.
    + (* b < 0 *)
      rewrite -oppr_gt0 => Nb_pos.
      have: exists A B, [/\ A \is_nb (-a)%:E, B \is_nb (-b)%:E
                          & forall x y, x ∈ A -> y ∈ B -> x * y ∈ filter_real_item (a * b)%:E n].
        rewrite -[a * b]mulrNN.
        by apply pos_pos.
      move=> [A [B [Anb Bnb ABW]]].

      have: exists U, U \is_nb a%:E /\ forall u, u ∈ U -> -u ∈ A.
        rewrite -[a%:E]oppeK.
        by apply filter_real_opp.
      move=> [U [Unb oppUA]].

      have: exists V, V \is_nb b%:E /\ forall v, v ∈ V -> -v ∈ B.
        rewrite -[b%:E]oppeK.
        by apply filter_real_opp.
      move=> [V [Vnb oppVB]].

      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -[u * v]mulrNN.
      apply ABW.
      ++ by apply oppUA.
      ++ by apply oppVB.
    + (* 0 < b *)
      move=> b_pos.
      have: exists A, A \is_nb (- a * b)%:E
                      /\ forall x, x ∈ A -> -x ∈ filter_real_item (a * b)%:E n.
        rewrite mulNr EFinN.
        by apply filter_real_opp.
      move=> [A [Anb oppAW]].

      have: exists B V, [/\ B \is_nb (- a)%:E, V \is_nb b%:E
                                               & forall x y, x ∈ B -> y ∈ V -> x * y ∈ A].
        by apply pos_pos.
      move=> [B [V [Bnb Vnb BVA]]].

      have: exists U, U \is_nb a%:E /\ forall x, x ∈ U -> -x ∈ B.
        rewrite -[a%:E]oppeK.
        by apply filter_real_opp.
      move=> [U [Unb oppUB]].

      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -[u * v]opprK -mulNr.
      apply oppAW.
      apply BVA => //.
      by apply oppUB.
  - (* 0 < a *)
    move=> a_pos.
    case: (real_trisection b 0).
    + (* b = 0 *)
      move ->.
      rewrite mulr0.
      have: exists V U, [/\ V \is_nb 0%E, U \is_nb a%:E
                          & forall x y, x ∈ V -> y ∈ U -> x * y ∈ filter_real_item 0 n].
        by apply zero_pos.
      move=> [V [U [Vnb Unb VUW]]].
      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite mulrC.
      by apply VUW.
    + (* b < 0 *)
      rewrite -oppr_gt0 => Nb_pos.
      have: exists A, A \is_nb (a * -b)%:E
                        /\ forall x, x ∈ A -> -x ∈ filter_real_item (a * b)%:E n.
        rewrite mulrN EFinN.
        by apply filter_real_opp.
      move=> [A [Anb oppAW]].
      have: exists U B, [/\ U \is_nb a%:E, B \is_nb (-b)%:E
                                           & forall x y, x ∈ U -> y ∈ B -> x * y ∈ A].
        by apply pos_pos.
      move=> [U [B [Unb Bnb UBA]]].
      have: exists V, V \is_nb b%:E /\ forall x, x ∈ V -> -x ∈ B.
      rewrite -[b%:E]oppeK.
         by apply filter_real_opp.
      move=> [V [Vnb oppVB]].

      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -[u * v]mulrNN mulNr.
      apply oppAW.
      apply UBA => //.
      by apply oppVB.
    + (* 0 < b *)
      move=> b_pos.
      apply pos_pos => //.
      by apply filter_real_contains_items. (* F1 hence the Hint Resolve doesn't work *)

(* final step: treat everything *)
move=> a b.
wlog le_ab: a b / (a <= b)%E => [asym_version|].
- case/orP: (le_total a b) ; first by apply asym_version.
  move=> le_ba ab_ok W Wnb.
  have: exists V U, [/\ V \is_nb b, U \is_nb a & forall x y, x ∈ V -> y ∈ U -> x * y ∈ W].
    apply asym_version => //.
    + by rewrite mule_defC.
    + by rewrite muleC.
  move => [V [U [Vnb Unb VUW]]].
  exists U.
  exists V.
  split=> //.
  move=> u v Uu Vv.
  rewrite mulrC.
  by apply VUW.
- move: le_ab. (* put it back so it's treated by the cases *)
  case: a => [ a | | ].
  + (* a is real *)
    case: b => [ b | | ].
    * (* b is real *)
      move=> _ _.
      by apply real_real.
    * (* b is +oo *)
      move=> _.
      case: (real_trisection a 0).
      -- (* a = 0 *)
        move=> ->.
        by rewrite /mule_def //= !eqxx //.
      -- (* a < 0 *)
        rewrite -oppr_gt0 => Na_pos _.
        have ->: (a%:E * +oo)%E = -oo%E :> \bar ℝ by rewrite lt0_muley // lte_fin -oppr_gt0.
        move=> W Wminfty.
        have: exists A, A \is_nb +oo%E /\ forall x, x ∈ A -> -x ∈ W.
          have ->: +oo%E = (- -oo)%E :> \bar ℝ by rewrite /oppe.
          by apply filter_real_opp.
        move=> [A [Anb oppAW]].

        have: exists B V, [/\ B \is_nb (-a)%:E, V \is_nb +oo & forall x y, x ∈ B -> y ∈ V -> x * y ∈ A].
          by apply pos_pinfty.
        move=> [B [V [Bnb Vnb BVA]]].
        have: exists U, U \is_nb a%:E /\ forall x, x ∈ U -> -x ∈ B.
          rewrite -[a]opprK EFinN.
          by apply filter_real_opp.
        move=> [U [Unb oppUB]].

        exists U.
        exists V.
        split=> //.
        move=> u v Uu Vv.
        rewrite -[u * v]opprK.
        apply oppAW.
        rewrite -mulNr.
        apply BVA => //.
        by apply oppUB.
      -- (* 0 < a *)
        move=> a_pos _.
        rewrite gt0_muley ; last by rewrite lte_fin.
        move=> W Wpinfty.
        by exact: pos_pinfty.
    * (* b is -oo *)
      by rewrite leeNy_eq //.
  + (* a is +oo *)
    case: b => [ b | | ].
    * (* b is real *)
      by rewrite leye_eq //.
    * (* b is +oo *)
      rewrite mulyy.
      move=> _ _.
      by exact pinfty_pinfty.
    * (* b is -oo *)
      by rewrite leye_eq //.
  + (* a is -oo *)
    case: b => [ b | | ].
    * (* b is real *)
      case: (real_trisection b 0).
      -- (* b = 0 *)
        move=> -> _.
        by rewrite /mule_def //= !eqxx //.
      -- (* b < 0 *)
        rewrite -oppr_gt0.
        move=> Nb_gt0 _ _.
        rewrite lt0_mulNye ; last  by rewrite lte_fin -oppr_gt0.
        move=> W Wnb.
        have: exists A B, [/\ A \is_nb (-b)%:E, B \is_nb +oo
                                                & forall x y, x ∈ A -> y ∈ B -> x * y ∈ W].
          by apply pos_pinfty.
        move=> [A [B [Anb Bnb ABW]]].
        rewrite EFinN in Anb.
        have: exists V, V \is_nb b%:E /\ forall x, x ∈ V -> -x ∈ A.
          rewrite -[b]opprK EFinN.
          by apply filter_real_opp.
        move=> [V [Vnb oppVA]].
        have: exists U, U \is_nb -oo /\ forall x, x ∈ U -> -x ∈ B.
          have ->: -oo = (- +oo)%E :> \bar ℝ by rewrite /oppe.
          by apply filter_real_opp.
        move=> [U [Unb oppUB]].

        exists U.
        exists V.
        split=> //.
        move=> u v Uu Vv.
        rewrite -[u * v]mulrNN mulrC.
        apply ABW.
        ** by apply oppVA.
        ** by apply oppUB.
      -- (* 0 < b *)
        move=> b_pos _ _.
        rewrite gt0_mulNye ; last by rewrite lte_fin.
        move=> W Wnb.
        have: exists A, A \is_nb +oo /\ forall x, x ∈ A -> -x ∈ W.
          have->: +oo = (- -oo)%E :> \bar ℝ by rewrite /oppe.
          by apply filter_real_opp.
        move=> [A [Anb oppAW]].
        have: exists V C, [/\ V \is_nb b%:E, C \is_nb +oo
                                             & forall x y, x ∈ V -> y ∈ C -> x * y ∈ A].
          by apply pos_pinfty.
        move=> [V [C [Vnb Cnb VCA]]].
        have: exists U, U \is_nb -oo /\ forall x, x ∈ U -> -x ∈ C.
          have ->: -oo = (- (+oo))%E :> \bar ℝ by rewrite /oppe.
          by apply filter_real_opp.
        move=> [U [Unb oppUC]].

        exists U.
        exists V.
        split=> //.
        move=> u v Uu Vv.
        rewrite -[u * v]opprK.
        rewrite mulrC.
        apply oppAW.
        rewrite -mulrN.
        apply VCA => //.
        by apply oppUC.
    * (* b is +oo *)
      move=> _ _.
      rewrite mulNyy.
      move=> W Wnb.
      have: exists A, A \is_nb +oo /\ forall x, x ∈ A -> -x ∈ W.
         have ->: +oo = (- (-oo))%E :> \bar ℝ by rewrite /oppe.
         by apply filter_real_opp.
      move=> [A [Anb oppAW]].

      have: exists B V, [/\ B \is_nb +oo, V \is_nb +oo
                                          & forall x y, x ∈ B -> y ∈ V -> x * y ∈ A].
        by apply pinfty_pinfty.
      move=> [B [V [Bnb Vnb BVA]]].

      have: exists U, U \is_nb -oo /\ forall x, x ∈ U -> -x ∈ B.
        have ->: -oo = (- +oo)%E :> \bar ℝ by rewrite /oppe.
        by apply filter_real_opp.
      move=> [U [Unb oppUB]].

      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -[u * v]opprK.
      apply oppAW.
      rewrite -mulNr.
      apply BVA => //.
      by apply oppUB.
    * (* b is -oo *)
      move=> _ _ W.
      rewrite mulNyNy => Wnb.
      have: exists A B, [/\ A \is_nb +oo, B \is_nb +oo
                                          & forall x y, x ∈ A -> y ∈ B -> x * y ∈ W].
        by apply pinfty_pinfty.
      move=> [A [B [Anb Bnb ABW]]].

      have: exists U, U \is_nb -oo /\ forall x, x ∈ U -> -x ∈ A.
        have ->: -oo = (- +oo)%E :> \bar ℝ by rewrite /oppe.
        by apply filter_real_opp.
      move=> [U [Unb oppUA]].

      have: exists V, V \is_nb -oo /\ forall x, x ∈ V -> -x ∈ B.
        have ->: -oo = (- +oo)%E :> \bar ℝ by rewrite /oppe.
        by apply filter_real_opp.
      move=> [V [Vnb oppVB]].

      exists U.
      exists V.
      split=> //.
      move=> u v Uu Vv.
      rewrite -mulrNN.
      apply ABW.
      -- by apply oppUA.
      -- by apply oppVB.
Qed.


(* RESULT: product of limits
   MC-A: cvgM in normedtype.v
   mathlib: TODO
*)

Lemma limit_real_mul
  (X: Type) (F: Filter X)
  (f: X -> ℝ) (a: \bar ℝ)
  (g: X -> ℝ) (b: \bar ℝ):
  a *? b -> f @ F --> a -> g @ F --> b
  -> f \* g @ F --> (a * b)%E.
Proof.
move=> ab_exists f_tends_to_a g_tends_to_b.
move=> W Wnb.

have: exists U V, [/\ U \is_nb a, V \is_nb b & forall x y, x ∈ U -> y ∈ V -> x * y ∈ W].
  by apply filter_real_mul.
move=> [U [V [Unb Vnb UVW]]].

have: [locally F, fun Sf => [set f x | x in Sf] ⊂ U].
  by apply f_tends_to_a.
move=> [Sf [Sfnb fSfU]]. (* F4 [locally ...] *)

have: [locally F, fun Sg => [set g x | x in Sg] ⊂ V].
  by apply g_tends_to_b.
move=> [Sg [Sgnb gSgV]]. (* F4 [locally ...] *)

exists (Sf ∩ Sg) ; split=> //.
rewrite image_sub /preimage.
move=> x.
rewrite -inE in_setI => /andP [xinSf xinSg]. (* F3 *)
rewrite -inE. (* F3 *)
apply UVW.
- in_superset fSfU.
  by apply imagePin.
- in_superset gSgV.
  by apply imagePin.
Qed.
