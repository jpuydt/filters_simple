(* This file is about shifting neighbourhoods by a constant *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum order archimedean.
From mathcomp Require Import boolp classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities filters.reals.definitions.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: shifting and neighbourhoods *)

Lemma filter_real_shift:
  forall (l: \bar ℝ) (b: ℝ),
  forall V, V \is_nb (l + b%:E)%E -> exists U, U \is_nb l /\ (forall u, u ∈ U -> (u + b) ∈ V).
Proof.
(* first drop to simpler neighbourhoods *)
move=> l b.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Anb AbU]].
  exists A; split=> //.
  move=> u Au.
  in_superset UsubV.
  by apply AbU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  case: l => [ l | | ].
+ (* l is a real *)
  rewrite -EFinD.
  exists n.
  move=> u.
  simpl. (* F1 *)
  rewrite !inE /mkset. (* F3 *)
  move=> uBnl.
  by rewrite [l + _]addrC addrKA.
+ (* l is +∞ *)
  rewrite addye //.
  pose q := Num.bound `|-b|.
  have bNleq: (-b <= q%:R)%R.
    rewrite /q ltW //.
    apply le_lt_trans with `|-b| ; first by rewrite ler_norm.
    by rewrite archi_boundP.
  exists (n + q)%N.
  move=> u.
  simpl. (* F1 *)
  rewrite !inE /mkset. (* F3 *)
  move=> uBn.
  rewrite -lerBDr.
  apply le_trans with (n+q)%:R => //.
  rewrite lerBDr.
  rewrite natrD -addrA lerDl.
  by rewrite -lerBDr add0r.
+ (* l is -∞ *)
  rewrite addNye.
  pose q := Num.bound `|b|.
  have b_le_q: (b <= q%:R).
    rewrite /q ltW //.
    apply le_lt_trans with `|b| ; first by rewrite ler_norm.
    by rewrite archi_boundP.
  exists (n + q)%N.
  move=> u.
  simpl. (* F1 *)
  rewrite !inE /mkset. (* F3 *)
  rewrite natrD addrA.
  move=> uBn.
  rewrite addrAC.
  apply le_trans with (u + n%:R + q%:R) => //.
  by rewrite lerD2l.
Qed.


(* RESULT: shift and real limit
   MC-A: cvg_comp_shift in normedtype.v
   mathlib: TODO
 *)

Lemma limit_real_shift
  (X: Type) (F: Filter X)
  (f: X -> ℝ) (l: \bar ℝ)
  (b: ℝ):
  f @ F --> l -> (fun x => f x + b) @ F --> (l + b%:E)%E.
Proof.
move=> f_tends_to_l.
move=> W Wnb.
have: exists U, U \is_nb l /\ forall u, u ∈ U -> u + b ∈ W.
  by apply filter_real_shift.
move=> [U [Unb UbW]].
have: [locally F, fun V => [set f x | x in V] ⊂ U].
  by apply f_tends_to_l.
move=> [V [Vnb fVU]]. (* F4 [locally ...] *)
exists V ; split=> //.
rewrite image_sub /preimage.
move=> x Vx.
rewrite -inE in Vx. (* F3 *)
rewrite -inE. (* F3 *)
apply UbW.
in_superset fVU.
by apply imagePin.
Qed.
