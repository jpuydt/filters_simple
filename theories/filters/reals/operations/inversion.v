(* This file is about inversion and real filters *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum archimedean order eqtype.
From mathcomp Require Import boolp classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.reals.definitions filters.reals.inequalities filters.reals.operations.opposite.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: inverse and neighbourhoods *)

Lemma filter_real_inv_finite:
  forall (a: ℝ), a != 0 -> forall W, W \is_nb a%:E
   -> exists V, V \is_nb (a^-1)%:E /\ (forall x, x ∈ V -> x != 0 /\ x^-1 ∈ W).
Proof.
move=> a.
wlog : a / (0 < a).
- (* prove the case 0 < a gives everything *)
  move=> case_gt0 a_neq0.
  (* of course it will directly give the case 0 < a ! *)
  case/orP: (lt_total a_neq0) ; last by move=> a_gt0 ; apply case_gt0.
  (* for the case a < 0, some more work on neighbourhoods is needed: we start
     from a neighbourhood W of a of course, but through opposite we get a neighbourhood
     V of -a ; and there we can get by inversion a neighbourhood B of (-a)^-1, and
     finally by another opposite a neighbourhood A of a^-1 with the right properties *)
  move=> a_lt0 W Wnb.
  have: exists V, V \is_nb (- a%:E)%E /\ forall x, x ∈ V -> -x ∈ W.
    by apply filter_real_opp.
  move=> [V [Vnb negVW]].

  have: exists B, B \is_nb (-a)^-1%:E /\ forall x, x ∈ B -> x != 0 /\ x^-1 ∈ V.
    by apply case_gt0 ; rewrite ?oppr_gt0 ?oppr_eq0.
  move=> [B [Bnb HB]].

  have: exists A, A \is_nb (a ^-1)%:E /\ forall x, x ∈ A -> -x ∈ B.
  rewrite -[a^-1]opprK EFinN -invrN.
    by apply filter_real_opp.
  move=> [A [Anb HA]].

  exists A; split=> //.
  move=> x Ax.

  have Bnegx: -x ∈ B by apply HA.
  have x_neq0: x != 0.
    rewrite -oppr_eq0.
    by apply HB.

  split => //.
  rewrite -[x^-1]opprK -invrN.
  apply negVW.
  by apply HB.

- (* now we need to actually prove the case 0 < a *)
   move=> a_gt0 a_neq0.
   have inva_gt0: 0 < a^-1 by rewrite invr_gt0.
   apply filter_from_sequential_family_local_prop. (* F1 *)
   + move=> U V Unba Vnba UsubV. (* F4 filter_from_sequential_family_local_prop *)
     move=> [A [Anb invAU]].
     exists A ; split=> //.
     move=> x Ax.
     split ; first by apply invAU.
     in_superset UsubV.
     by apply invAU.
   + move=> n.

     (* zero might still  be between a - 1/2^n and a + 1/2^n,
        so we need to shrink further with a 1/2^m radius *)

     pose m := (n + Num.bound a ^-1)%N.

     have n_lt_m: (n < m)%N by rewrite -[n]addn0 /m ltn_add2l. (* FIXME: ugly *)

     suff: exists V, V \is_nb (a^-1)%:E
                     /\ forall x, x ∈ V -> x !=0 /\ x^-1 ∈ seq_item (filter_real_sequential a%:E) m.
       move=> [V [Vnb HV]].
       exists V ; split=> //.
       move=> x xinV.
       split ; first by apply HV.
       (* FIXME: ugly... *)
       have BmBn := @seq_item_nonincr ℝ (filter_real_sequential a%:E) n m (ltnW n_lt_m).
       in_superset BmBn.
       by apply HV.

     (* indeed zero isn't a problem anymore *)

     have aNexp2m_gt0: 0 < a - 1 / 2 ^+ m.
       rewrite subr_gt0.
       rewrite -my_ltr_pV2 ; rewrite ?inE // ; last by rewrite div1r invr_gt0 exprn_gt0.
       rewrite div1r invrK.
       apply lt_trans with m%:R ; last by rewrite -natrX ltr_nat n_lt_exp2n.
       rewrite /m natrD ltr_wpDl //.
       by rewrite archi_boundP // ltW.

     have aexp2m_gt0: 0 < a + 1 / 2 ^+ m.
       by rewrite addr_gt0 // div1r invr_gt0 exprn_gt0.

     have l_inv_bound_ok: 0 < a^-1 - 1 / (a + 1 / 2 ^+ m).
       rewrite subr_gt0.
       rewrite div1r my_ltr_pV2 ; rewrite ?inE //.
       rewrite ltr_pwDr => //.
       by rewrite div1r invr_gt0 exprn_gt0 // ltr0n.

     have l_bound_ok: 0 < 1 / (a^-1 - 1 / (a + 1 / 2 ^+ m)).
       by rewrite div1r invr_gt0.

     have r_inv_bound_ok: 0 < 1 / (a - 1 / 2 ^+ m) - a^-1.
       rewrite subr_gt0.
       rewrite div1r my_ltr_pV2 ; rewrite ?inE //.
       rewrite ltrBDr ltr_pwDr //.
       by rewrite div1r invr_gt0 exprn_gt0 // ltr0n.

     have r_bound_ok: 0 < 1 / (1 / (a - 1 / 2 ^+ m) - a^-1).
       by rewrite div1r invr_gt0.

    (* now we are able to control things left and right *)

    pose l := Num.bound (1/ (a^-1 - 1 / (a + 1 / 2 ^+ m))).
    pose r := Num.bound (1/ (1 / (a - 1 / 2 ^+ m) - a^-1)).
    pose q := (l + r)%N. (* common good bound *)

   (* prepare quite a few inequalities *)

    have l_le_q: (l <= q)%N.
      by rewrite leq_addr.

    have r_le_q: (r <= q)%N.
      by rewrite leq_addl.

    have invaNexp2Nq_gt0: 0 < a^-1 - 1 / 2 ^+ q.
      apply lt_trans with ((1 / (a + 1 / 2 ^+ m))).
        by rewrite div1r invr_gt0.
      rewrite ltrBDr -ltrBDl.
      rewrite -my_ltr_pV2 ; rewrite ?inE // ; first last.
        by rewrite div1r invr_gt0 exprn_gt0 // ltr0n.
      rewrite -div1r.
      apply lt_trans with l%:R.
        by rewrite /l archi_boundP // ltW.
     rewrite -/l div1r invrK.
     apply lt_le_trans with (2 ^+ l) ; first by rewrite -natrX ltr_nat n_lt_exp2n.
     by apply exp2n_nondecr.

    (* now we get rolling *)

    apply filter_from_sequential_family_exists. (* F1 *)
    exists q.
    move=> x.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite ler_distl.
    move/andP => [x_ge x_le].
    split.
    + rewrite lt0r_neq0 //.
      by apply lt_le_trans with (a^-1 - 1 / 2 ^+ q).

    + have invx_gt0: 0 < x^-1.
        rewrite invr_gt0.
        by apply lt_le_trans with (a^-1 - 1 / 2  ^+ q).


      rewrite ler_distl.
      apply/andP ; split.
      -- rewrite -my_ler_pV2 ; rewrite ?inE //.
         rewrite invrK -div1r.
         apply le_trans with (a^-1 + 1 / 2 ^+ q) => //.
         rewrite ltW // -ltrBDl.
         rewrite -my_ltr_pV2 ; rewrite ?inE // ; last by rewrite div1r invr_gt0 exprn_gt0.
         apply lt_trans with r%:R; first by rewrite -div1r /r archi_boundP // ltW.
         rewrite div1r invrK.
         rewrite -natrX ltr_nat.
         apply ltn_leq_trans with (2 ^ r) ; first by apply n_lt_exp2n.
         by rewrite -(ler_nat ℝ) !natrX exp2n_nondecr. (* FIXME: bad *)
      -- rewrite -my_ler_pV2 ; rewrite ?inE //.
         rewrite invrK.
         apply le_trans with (a^-1 - 1 / 2 ^+ q) => //.
         rewrite -div1r ltW //.
         rewrite ltrBDl -ltrBDr.
         rewrite -my_ltr_pV2 ; rewrite ?inE // ; last by rewrite div1r invr_gt0 exprn_gt0.
         apply lt_trans with l%:R ; first by rewrite -div1r /l archi_boundP // ltW.
         rewrite div1r invrK.
         rewrite -natrX ltr_nat.
         apply ltn_leq_trans with (2 ^ l) ; first by apply n_lt_exp2n.
         by rewrite -(ler_nat ℝ) !natrX exp2n_nondecr. (* FIXME: bad *)
Qed.



Lemma filter_real_inv_pinfty:
  forall W, W \is_nb +oo
   -> exists V, V \is_nb (𝒩[>] 0) /\ (forall x, x ∈ V -> x != 0 /\ x^-1 ∈ W).
Proof.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Uy Vy UsubV. (* F4 filter_prop_local *)
  move=> [A [Anb invAU]].
  exists A; split=> //.
  move=> x Ax.
  split ; first by apply invAU.
  in_superset UsubV.
  by apply invAU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  exists n.
  move=> x.
  rewrite !inE /mkset //=. (* F3 *)
  rewrite subr0.
  move => [xge xgt0].
  rewrite gtr0_norm // in xge.
  split ; first by apply lt0r_neq0.
  apply le_trans with (2 ^+ n); first by rewrite -natrX ltW // ltr_nat n_lt_exp2n.
  rewrite -my_ler_pV2.
  + by rewrite invrK -div1r.
  + by rewrite inE invr_gt0.
  + by rewrite inE exprn_gt0.
Qed.



Lemma filter_real_inv_minfty:
  forall W, W \is_nb -oo
   -> exists V, V \is_nb (𝒩[<] 0) /\ (forall x, x ∈ V -> x != 0 /\ x^-1 ∈ W).
Proof.
have ->: -oo = (- +oo)%E :> \bar ℝ by rewrite /oppe.
move=> W Wnb.
have: exists V, V \is_nb (- - +oo)%E /\ forall x, x ∈ V -> - x ∈ W by apply filter_real_opp.
rewrite oppeK.
move=> [V [Vnb negVW]].
have: exists U, U \is_nb (𝒩[>] 0) /\ forall x, x ∈ U -> x != 0 /\ x ^-1 ∈ V by apply filter_real_inv_pinfty.
move=> [U [Unb UinvV]].
have: exists T, T \is_nb (𝒩[<] -0) /\ forall x, x ∈ T -> -x ∈ U by apply filter_real_gt_opp.
move=> [T [Tnb negTU]].
rewrite oppr0 in Tnb.
exists T; split=> //.
move=> x Tx.
split.
- rewrite -oppr_eq0.
  apply UinvV.
  by apply negTU.
- rewrite -[x ^-1]opprK.
  apply negVW.
  rewrite -invrN.
  apply UinvV.
  by apply negTU.
Qed.



Lemma filter_real_inv_pzero:
  forall W, W \is_nb (𝒩[>] 0)
   -> exists V, V \is_nb +oo /\ forall x, x ∈ V -> x != 0 /\ x^-1 ∈ W.
Proof.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Ay invAU]].
  exists A ; split=> //.
  move=> x Ax.
  split ; first by apply invAU.
  in_superset UsubV.
  by apply invAU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  exists (2 ^ n.+1).
  move=> x.
  rewrite !inE /mkset //=. (* F3 *)
  rewrite subr0.
  move=> xge.
  have x_gt0: 0 < x.
    apply lt_le_trans with (2 ^ n.+1)%:R => //.
    by rewrite natrX exprn_gt0.
  split; first by rewrite lt0r_neq0.
  split ; last by rewrite invr_gt0.
  rewrite div1r gtr0_norm ; last by rewrite invr_gt0.
  rewrite my_ler_pV2 ; rewrite ?inE ?exprn_gt0 ?ltr0n //.
  apply le_trans with (2 ^ n.+1)%:R => //.
  by rewrite natrX ltW // exp2n_incr.
Qed.



(* RESULT: inverse of finite real limit *)

Lemma limit_real_inv_finite
  {X: Type} {F: Filter X} (f: X -> ℝ):
  forall (a: ℝ), a != 0 -> f @ F --> a%:E -> (fun x => (f x)^-1) @ F --> (a^-1)%:E.
Proof.
move=> a a_neq0 f_at_F_a.
apply filter_tendsto_to_sequential. (* F1 *)
move=> n.
have inva_neq0: a^-1 != 0 by rewrite invr_eq0.
have: exists W, W \is_nb (a ^-1 ^-1)%:E
                  /\ forall x, x ∈ W -> x !=0 /\ x^-1 ∈ filter_real_item (a ^-1)%:E n.
  by apply filter_real_inv_finite.
rewrite invrK.
move=> [W [Wnb invWBn]].
have: [locally F, fun V => [set f x | x in V] ⊂ W] by apply f_at_F_a.
move=> [V [Vnb invVBn]]. (* F4 [locally ...] *)
rewrite image_sub /preimage in invVBn.
exists V; split=> //.
rewrite image_sub /preimage.
move=> x Vx.
rewrite -inE in Vx. (* F3 *)
rewrite -inE. (* F3 *)
apply invWBn.
by in_superset invVBn.
Qed.



(* RESULT: inverse of pinfty real limit *)

Lemma limit_real_inv_pinfty
  {X: Type} {F: Filter X} (f: X -> ℝ):
  f @ F --> +oo -> (fun x => (f x)^-1) @ F --> 𝒩[>] 0.
Proof.
move=> f_at_F_pinfty.
apply filter_tendsto_to_sequential. (* F1 *)
move=> n.
have: exists W, W \is_nb +oo /\ forall x, x ∈ W -> x != 0 /\ x^-1 ∈ filter_real_gt_item 0 n.
  by apply filter_real_inv_pzero.
move=> [W [Wnb invWBn]].
move: invWBn ; simpl. (* F1 *)
move=> invWBn.
have: [locally F, fun V => [set f x | x in V] ⊂ W] by apply f_at_F_pinfty.
move=> [V [Vnb fVW]]. (* F4 [locally ...] *)
exists V ; split=> //.
rewrite image_sub /preimage.
move=> x Vx.
rewrite -inE in Vx. (* F3 *)
rewrite -inE. (* F3 *)
apply invWBn.
in_superset fVW.
by rewrite imagePin.
Qed.
