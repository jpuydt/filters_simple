(* This file is about scaling real neighbourhoods *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum order archimedean.
From mathcomp Require Import boolp classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities filters.reals.definitions.

Import Bool Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: scaling and neighbourhoods *)

Lemma filter_real_scale:
  forall (b: \bar ℝ), forall a: ℝ,
  a%:E *? b -> forall V, V \is_nb (a%:E * b)%E
  -> exists U, U \is_nb b /\ forall u, u ∈ U -> a * u ∈ V.
Proof.
(* use simpler neighbourhoods *)
move=> b a amulb.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Anb aAU]].
  exists A; split=> //.
  move=> u Au.
  in_superset UsubV.
  by apply aAU.
- move=> n.
  move: amulb. (* put back so the case can treat it *)
  case: b => [ b | | ].
  + (* b is real *)
    move=> _.
    pose q := Num.bound `|a|.
    apply filter_from_sequential_family_exists. (* F1 *)
    exists (n + q)%N.
    move=> u.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> Bnqu.
    rewrite -mulrBr normrM.
    have: `|a| <= (2 ^ q)%:R.
      rewrite ltW //.
      apply lt_le_trans with q%:R ; first by apply archi_boundP.
      by rewrite ltW // ltr_nat n_lt_exp2n.
    move=> ale2q.
    calc_on (`|a| * `|u -b|).
    calc_le ((2 ^ q)%:R * (1 / 2 ^+ (n + q)) : ℝ).
      by rewrite ler_pM.
    calc_eq ((2 ^+ q) / 2 ^+ (n + q): ℝ).
      by rewrite natrX mulrCA mul1r.
    calc_eq (1 / 2 ^+ n : ℝ).
      (* FIXME: how many ugly lines for a trivial equality?! *)
      apply (@mulfI _ (2 ^+ (n + q) : ℝ)) ; first by rewrite lt0r_neq0 // exprn_gt0.
      rewrite mul1r.
      rewrite mulrCA divff ; last by rewrite lt0r_neq0 // exprn_gt0.
      rewrite mulr1.
      rewrite -expfB_cond. (* FIXME: makes a  bool + ℕ appear -- this is so wrong! *)
      * by rewrite addKn.
      * rewrite (negPf (lt0r_neq0 _)) ; last by rewrite ltr0n. (* FIXME: sigh... *)
        by rewrite add0n leq_addr.
    by [].
+ (* b is +∞ *)
  rewrite /mule_def.
  rewrite orb_false_r andb_true_r. (* FIXME: not automatic !? *)
  rewrite eqe => aneq0.
  case/orP: (lt_total aneq0).
  * (* a < 0 *)
    move=> alt0.
    have ->: (a%:E * +oo)%E = -oo :> \bar ℝ.
      by rewrite mulr_infty ltr0_sg // mulN1e /oppe. (* FIXME: too hard *)
    pose q := Num.bound ((- a) ^-1).
    apply filter_from_sequential_family_exists. (* F1 *)
    exists (q * n)%N.
    move=> u.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> Hu.
    rewrite -lerBDl sub0r -mulNr.
    rewrite -ler_pdivrMl ; last by rewrite oppr_gt0.
    apply le_trans with (q * n)%:R => //.
    rewrite natrM.
    apply ler_pM => //.
    -- by rewrite ltW // invr_gt0 oppr_gt0.
    -- by rewrite ltW // archi_boundP // ltW // invr_gt0 oppr_gt0.
  * (* a > 0 *)
    move=> agt0.
    have ->: (a%:E * +oo)%E = +oo :> \bar ℝ.
      by rewrite mulr_infty gtr0_sg // mul1e. (* FIXME: too hard *)
    pose q := Num.bound (a ^-1).
    apply filter_from_sequential_family_exists. (* F1 *)
    exists (q * n)%N.
    move=> u.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> Hu.
    rewrite -ler_pdivrMl //.
    apply le_trans with (q * n)%:R => //.
    rewrite natrM.
    apply ler_pM => // ; first by rewrite ltW // invr_gt0.
    by rewrite ltW // archi_boundP // ltW // invr_gt0.
+ (* b is -∞ *)
  rewrite /mule_def.
  rewrite orb_false_r andb_true_r. (* FIXME: not automatic !? *)
  rewrite eqe => aneq0.
  case/orP: (lt_total aneq0).
  * (* a < 0 *)
    move=> alt0.
    have ->: (a%:E * -oo)%E = +oo :> \bar ℝ.
      by rewrite mulr_infty ltr0_sg // mulN1e /oppe. (* FIXME: too hard *)
    pose q := Num.bound ((- a) ^-1).
    apply filter_from_sequential_family_exists. (* F1 *)
    exists (q * n)%N.
    move=> u.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> Hu.
    rewrite -subr_le0 addrC -mulNr.
    have ->: n%:R = -a * ((-a) ^-1 * n%:R).
      rewrite mulrA divrr ; first by rewrite mul1r.
      by rewrite unitfE oppr_eq0.
    rewrite -mulrDr.
    rewrite pmulr_rle0 ; last by rewrite oppr_gt0.
    apply le_trans with( u + (q * n)%:R) => //.
    rewrite lerD2l natrM.
    apply ler_pM => //.
    -- by rewrite ltW // invr_gt0 oppr_gt0.
    -- by rewrite ltW // archi_boundP // ltW // invr_gt0 oppr_gt0.
  * (* a > 0 *)
    move=> agt0.
    have ->: (a%:E * -oo)%E = -oo :> \bar ℝ.
      by rewrite mulr_infty gtr0_sg // mul1e.
    pose q := Num.bound (a ^-1).
    apply filter_from_sequential_family_exists. (* F1 *)
    exists (q * n)%N.
    move=> u.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> Hu.
    have ->: n%:R = a * (a ^-1 * n%:R).
      rewrite mulrA.
      rewrite divrr ; first by rewrite mul1r.
      by rewrite unitfE.
    rewrite -mulrDr.
    rewrite pmulr_rle0 //.
    apply le_trans with (u + (q * n)%:R) => //.
    rewrite lerD2l natrM.
    apply ler_pM => //.
    -- by rewrite ltW // invr_gt0.
    -- by rewrite ltW // archi_boundP // ltW // invr_gt0.
Qed.



(* RESULT: scaling and limits
   MC-A: TODO
   mathlib: TODO
*)
Lemma limit_real_scale
  (X: Type) (F: Filter X)
  (f: X -> ℝ) (l: \bar ℝ) (a: ℝ):
  a%:E *? l -> f @ F --> l -> (fun x => a * f x) @ F --> (a%:E * l)%E.
Proof.
move=> al_exists f_tends_to_l.
move=> W Wnb.
have: exists V, V \is_nb l /\ forall v, v ∈ V -> a * v ∈ W.
  by apply filter_real_scale.
move=> [V [Vnb aVW]].
have: [locally F, fun U => [set f x | x in U] ⊂ V].
  by apply f_tends_to_l.
move=> [U [Unb fUV]]. (* F4 [locally ...] *)
exists U ; split=> //.
rewrite image_sub /preimage.
move=> x Ux.
rewrite -inE in Ux. (* F3 *)
rewrite -inE. (* F3 *)
apply aVW.
in_superset fUV.
by apply imagePin.
Qed.
