(* This file is about the opposite operation on real neighbourhoods *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnum.
From mathcomp Require Import boolp classical_sets constructive_ereal mathcomp_extra reals.

From Ici Require Import missing filters.generalities filters.reals.definitions.

Import Bool Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: opposite and neighbourhoods *)

Lemma filter_real_opp:
  forall (l: \bar ℝ), forall W, W \is_nb l
   -> exists V, V \is_nb (-l)%E /\ (forall x, x ∈ V ->  -x ∈ W).
Proof.
move=> l.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV [A [Anb NAU]]. (* F4 filter_from_sequential_family_local_prop *)
  exists A ; split=> //.
  move=> x ANx.
  in_superset UsubV.
  by apply NAU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  exists n.
  move=> x.
  case: l.
  + (* l is real *)
    move=> l.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite opprK => xnearl.
    by rewrite -normrN opprD !opprK.
  + (* l is +oo *)
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite /oppe.
    move=> xn_le0.
    by rewrite -subr_le0 opprK addrC.
  + (* l is -oo *)
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite /oppe.
    move => nlex.
    by rewrite addrC subr_le0.
Qed.



(* Result: opposite and limits
   MC-A: cvgN, cvgNry, cvgNrNy in normedtype.v
   mathlib: TODO
 *)

Lemma limit_real_opp
  (X: Type) (F: Filter X)
  (f: X -> ℝ) (l: \bar ℝ):
  f @ F --> l -> \- f @ F --> (-l)%E.
Proof.
move=> f_tends_to_l.
move=> W Wnb. (* F4 filter_tendsto *)
unfold GRing.opp_fun. (* F2 *)
have: exists U, U \is_nb l%E /\ forall x, x ∈ U -> -x ∈ W.
  rewrite -[l]oppeK.
  by apply filter_real_opp.
move => [U [Ul oppUW]].
have: [locally F, fun V => [set f x| x in V] ⊂ U].
  by apply f_tends_to_l.
move=> [V [Vnb fVU]]. (* F4 [locally ...] *)
exists V ; split=> //.
rewrite image_sub /preimage.
move=> x Vx.
rewrite -inE in Vx. (* F3 *)
rewrite /mkset.
rewrite -inE. (* F3 *)
apply oppUW.
in_superset fVU.
by apply imagePin.
Qed.



(* RESULT: opposite and strict left neighbourhoods *)

Lemma filter_real_lt_opp:
  forall (l: ℝ), forall W, W \is_nb (𝒩[<] l)
   -> exists V, V \is_nb (𝒩[>] (-l)) /\ (forall x, x ∈ V ->  -x ∈ W).
Proof.
move=> l.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Anb negAU]].
  exists A; split=> //.
  move=> x Ax.
  in_superset UsubV.
  by apply negAU.
- move=> n.
  apply filter_from_sequential_family_exists.
  exists n.
  move=> x.
  rewrite !inE /mkset //=. (* F3 *)
  rewrite opprK -opprD normrN.
  move => [xlbound xgt].
  split => //=.
  by rewrite ltrNl.
Qed.



(* RESULT: opposite and left neighbourhoods *)

Lemma filter_real_le_opp:
  forall (l: ℝ), forall W, W \is_nb (𝒩[≤] l)
   -> exists V, V \is_nb (𝒩[≥] (-l)) /\ (forall x, x ∈ V ->  -x ∈ W).
Proof.
move=> l.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Anb negAU]].
  exists A; split=> //.
  move=> x Ax.
  in_superset UsubV.
  by apply negAU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  exists n.
  move=> x.
  rewrite !inE /mkset //=. (* F3 *)
  rewrite opprK -opprD normrN.
  move => [xlbound xge].
  split => //=.
  by rewrite lerNl.
Qed.



(* RESULT: opposite and right neighbourhoods *)

Lemma filter_real_ge_opp:
  forall (l: ℝ), forall W, W \is_nb (𝒩[≥] l)
   -> exists V, V \is_nb (𝒩[≤] (-l)) /\ (forall x, x ∈ V ->  -x ∈ W).
Proof.
move=> l.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Anb negAU]].
  exists A; split=> //.
  move=> x Ax.
  in_superset UsubV.
  by apply negAU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  exists n.
  move=> x.
  rewrite !inE /mkset //=. (* F3 *)
  rewrite -opprD opprK normrN.
  move => [xlbound xle].
  split => //=.
  by rewrite lerNr.
Qed.



(* RESULT: opposite and strict right neighbourhoods *)

Lemma filter_real_gt_opp:
  forall (l: ℝ), forall W, W \is_nb (𝒩[>] l)
   -> exists V, V \is_nb (𝒩[<] (-l)) /\ (forall x, x ∈ V ->  -x ∈ W).
Proof.
move=> l.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_from_sequential_family_local_prop *)
  move=> [A [Anb negAU]].
  exists A; split=> //.
  move=> x Ax.
  in_superset UsubV.
  by apply negAU.
- move=> n.
  apply filter_from_sequential_family_exists. (* F1 *)
  exists n.
  move=> x.
  rewrite !inE /mkset //=. (* F3 *)
  rewrite -opprD opprK normrN.
  move => [xlbound xlt].
  split => //=.
  by rewrite ltrNr.
Qed.
