(* This file is about addition and real filters *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum archimedean order.
From mathcomp.algebra_tactics Require Import ring.
From mathcomp Require Import boolp classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities filters.reals.definitions.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: addition and neighbourhoods *)

Lemma filter_real_finite_add:
  forall (a b: ℝ), forall W: set ℝ,
    W \is_nb (a + b)%:E
    -> exists U V, [/\ U \is_nb a%:E,
                       V \is_nb b%:E
                     & forall u v, u ∈ U -> v ∈ V -> u + v ∈ W].
Proof.
move=> a b W Wnb.
move: Wnb => [n HnW].
simpl in HnW. (* F1 *)
apply filter_from_sequential_family_exists2.
exists (n + 1).
exists (n + 1).
move=> u v unba vnbb.
rewrite inE //= in unba.
rewrite inE //= in vnbb.
apply (in_superset HnW).
rewrite inE //=. (* F3 *)
have ->: (u+v)%E - (a + b)%E = (u - a) + (v - b) by ring.
apply le_trans with (`|u - a| + `|v - b|) ; first by rewrite ler_normD.
have <-: 1 / 2 ^+ (n + 1) + 1 / 2 ^+ (n + 1) = 1 / 2 ^+ n :> ℝ ; last by apply lerD.
(* FIXME: how many ugly lines for a trivial equality?! *)
apply (@mulfI _ (2 ^+ (n + 1) : ℝ)) ; first by rewrite exp2n_neq0.
rewrite mulrDr mul1r divff ; last by rewrite exp2n_neq0.
rewrite mul1r -expfB // ; last by rewrite addn1.
by rewrite addn1 subSnn expr1.
Qed.

Lemma filter_real_plus_infinite_add:
  forall a : ℝ, forall W: set ℝ,
    W \is_nb +oo
    -> exists U V, [/\ U \is_nb a%:E,
                       V \is_nb +oo
                     & forall u v, u ∈ U -> v ∈ V -> u + v ∈ W].
Proof.
move=> a W [n HnW]. (* F1 *)
simpl in HnW. (* F1 *)
pose q := Num.bound `|-a|.
apply filter_from_sequential_family_exists2.
exists 0.
exists (n + q + 1).
move=> u v unba vnby.
rewrite inE //= expr0 invr1 mul1r in unba. (* F1 *)
rewrite inE //= in vnby. (* F3 *)
apply (in_superset HnW).
rewrite inE //=. (* F1 *)
move: (ler_distlCBl unba) => uge.
apply le_trans with ((a - 1) + (n + q + 1)%:R) ; last by rewrite lerD.
have ->: (a - 1) + (n + q + 1)%:R = n%:R + (a + q%:R) by ring.
rewrite -[X in X <= _]addr0.
rewrite lerD //.
rewrite -lerBDl add0r.
apply le_trans with `|-a| ; first by rewrite ler_norm.
by rewrite ltW // archi_boundP.
Qed.

Lemma filter_real_minus_infinite_add:
  forall a : ℝ, forall W: set ℝ,
    W \is_nb -oo
    -> exists U V, [/\ U \is_nb a%:E,
                       V \is_nb -oo
                     & forall u v, u ∈ U -> v ∈ V -> u + v ∈ W].
Proof.
move=> a W [n HnW]. (* F1 *)
simpl in HnW. (* F1 *)
pose q := Num.bound `|a|.
apply filter_from_sequential_family_exists2.
exists 0.
exists (n + q + 1).
move=> u v unba vnby.
rewrite inE //= expr0 invr1 mul1r in unba. (* F1 *)
rewrite inE //= in vnby. (* F3 *)
apply (in_superset HnW).
rewrite inE //=. (* F1 *)
move: (ler_distlDr unba) => uge.
move: vnby.
have ->: v + (n + q + 1)%:R = (v + n%:R) + (q + 1)%:R by ring.
rewrite -lerBrDr sub0r.
move=> vnby.
rewrite -addrA.
apply le_trans with ((a + 1%R) - (q + 1)%:R) ; first by rewrite lerD.
have ->: (a + 1) - (q + 1)%:R = a - q%:R by ring.
rewrite subr_le0.
apply le_trans with `|a| ; first by rewrite ler_norm.
by rewrite ltW // archi_boundP.
Qed.


Lemma filter_real_add:
  forall (a b: \bar ℝ),
  a +? b
  -> forall W: set ℝ, W \is_nb (a + b)%E
  -> exists U V, [/\ U \is_nb a,
                     V \is_nb b
                   & forall u v, u ∈ U -> v ∈ V -> u + v ∈ W].
Proof.
move=> a b.
case: a => [a | | ].
- (* a is real *)
  case: b => [b | | ].
  + (* b is real *)
    move=> _. (* the condition a +? b is moot *)
    by apply filter_real_finite_add.
  + (* b is +oo *)
    move=> _. (* the condition a +? b is moot *)
    rewrite addey //.
    by apply filter_real_plus_infinite_add.
  + (* b is -oo *)
    move=> _. (* the condition a +? b is moot *)
    rewrite addeNy //.
    by apply filter_real_minus_infinite_add.
- (* a is +oo *)
  case: b => [b | | // ].
  + (* b is real *)
    move=> _. (* the condition a +? b is moot *)
    rewrite addye //.
    move=> W Wnb.
    move: (filter_real_plus_infinite_add b Wnb) => [V [U [Vnb Unb VaddU]]].
    exists U.
    exists V.
    split=> //.
    move=> u v uU vV.
    rewrite addrC.
    by apply VaddU.
  + (* b is +oo *)
    move=> _. (* the condition a +? b is moot *)
    rewrite addey //.
    move=> W [n HnW]. (* F1 *)
    simpl in HnW. (* F1 *)
    apply filter_from_sequential_family_exists2.
    exists n.
    exists 0.
    move=> u v.
    rewrite 2?inE //=. (* F3 *)
    move=> uge vge.
    apply (in_superset HnW).
    rewrite inE //=. (* F3 *)
    rewrite -[X in X <= _]addr0.
    by apply lerD.
- (* a is -oo *)
  case: b => [b | // | ].
  + (* b is real *)
    move=> _. (* the condition a +? b is moot *)
    rewrite addNye //.
    move=> W Wnb.
    move: (filter_real_minus_infinite_add b Wnb) => [V [U [Vnb Unb VaddU]]].
    exists U.
    exists V.
    split=> //.
    move=> u v uU vV.
    rewrite addrC.
    by apply VaddU.
  + (* b is -oo *)
    move=> _. (* the condition a +? b is moot *)
    move=> W [n HnW]. (* F1 *)
    simpl in HnW. (* F1 *)
    apply filter_from_sequential_family_exists2.
    exists 0.
    exists n.
    move=> u v.
    rewrite 2?inE //=. (* F3 *)
    move=> ule vle.
    rewrite addr0 in ule.
    apply (in_superset HnW).
    rewrite inE //=. (* F3 *)
    rewrite -[X in _ <= X]addr0.
    rewrite -addrA.
    by apply lerD.    
Qed.



(* RESULT: addition of limits
   MC-A: cvgD in normedtype.v
   mathlib: TODO
*)

Lemma limit_real_add
  (X: Type) (F: Filter X)
  (f: X -> ℝ) (a: \bar ℝ)
  (g: X -> ℝ) (b: \bar ℝ):
  a +? b -> f @ F --> a -> g @ F --> b
  -> f \+ g @ F --> (a + b)%E.
Proof.
move=> ab_exists f_tends_to_a g_tends_to_b.
move=> W Wnb. (* F4 filter_tendsto *)
have: exists U V, [/\ U \is_nb a, V \is_nb b & forall u v, u ∈ U -> v ∈ V -> u + v ∈ W].
  by apply filter_real_add.
move=> [U [V [Unb Vnb UVW]]].
have: [locally F, fun Sf => [set f x | x in Sf] ⊂ U].
  by apply f_tends_to_a.
move=> [Sf [Sfnb fSfU]]. (* F4 [locally ...] *)
have: [locally F, fun Sg => [set g x | x in Sg] ⊂ V].
  by apply g_tends_to_b.
move=> [Sg [Sgnb gSgV]]. (* F4 [locally ...] *)
exists (Sf ∩ Sg); split=> //.
rewrite image_sub /preimage.
move=> x [xinSf xinSg].
rewrite -inE in xinSf. (* F3 *)
rewrite -inE in xinSg. (* F3 *)
rewrite -inE. (* F3 *)
apply UVW.
- in_superset fSfU.
  by apply imagePin.
- in_superset gSgV.
  by apply imagePin.
Qed.
