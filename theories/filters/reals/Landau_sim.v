(* This file is about the Landau ∼ notation *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnum order eqtype.
From mathcomp.algebra_tactics Require Import ring.
From mathcomp Require Import classical_sets boolp functions reals constructive_ereal.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.nat.
From Ici Require Import filters.reals.definitions filters.reals.operations.
From Ici Require Import filters.reals.Landau_O filters.reals.Landau_o.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: Landau sim notation for real-valued functions *)

Definition filter_real_Landau_sim
  {X: Type} (F: Filter X) (f g: X -> ℝ) :=
  f \- g =o_F g.

Reserved Notation "f '∼_' F g" (at level 70, no associativity, F at level 0, g at next level).
Notation "f '∼_' F g" := (filter_real_Landau_sim F f g).



(* RESULT: Landau sim means replaceable in Landau O
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_sim_in_O
  {X: Type} (F: Filter X) (f g h1 h2: X -> ℝ):
  f = g +O_F h1 -> h1 ∼_F h2 -> f = g +O_F h2.
Proof.
move=> [M [M_gt0 [U [Unb HU]]]]. (* F4 =O and {near ...} *)
move=> h1simh2.
have: {near F, fun x => `|(h1 \- h2) x| <= 1 * `|h2 x|}.
  by apply h1simh2.
move=> [V [Vnb HV]]. (* F4 {near ...} *)
exists (2 * M).
split ; first by rewrite mulr_gt0.
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (M * `|h1 x|); first by apply HU.
rewrite -[leRHS]mulrAC mulrC.
rewrite ler_pM // ; first by apply ltW.
have ->: h1 x = (h1 \- h2) x + h2 x.
  rewrite /GRing.sub_fun. (* F2 *)
  by ring.
calc_on (`|(h1 \- h2) x + h2 x|).
calc_le (`|(h1 \- h2) x| + `|h2 x|).
  by apply ler_normD.
calc_le (`|h2 x| + `|h2 x|).
  rewrite lerD2r.
  rewrite -[leRHS]mul1r.
  by apply HV.
calc_le (2 * `|h2 x|).
  by rewrite mulr2n mulrDl mul1r.
by [].
Qed.



(* RESULT: Landau sim means replaceable in Landau O
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_sim_in_o
  {X: Type} (F: Filter X) (f g h1 h2: X -> ℝ):
  f = g +o_F h1 -> h1 ∼_F h2 -> f = g +o_F h2.
Proof.
move=> fgoh1 h1simh2.
move=> ϵ ϵ_gt0.
have twoϵ_gt0: 0 < ϵ / 2 by rewrite divr_gt0.
have: {near F, fun x => `|(f \- g) x| <= ϵ / 2 * `|h1 x|}.
  by apply fgoh1.
move=> [U [Unb HU]]. (* F4 {near ...} *)
have: {near F, fun x => `|(h1 \- h2) x| <= 1 * `|h2 x|}.
  by apply h1simh2.
move=> [V [Vnb HV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (ϵ / 2 * `|h1 x|); first by apply HU.
rewrite -mulrA.
rewrite ler_pM //.
- by apply ltW.
- apply mulr_ge0.
  + by rewrite invr_ge0.
  + by rewrite normr_ge0.
rewrite mulrC.
rewrite ler_pdivrMr //.
rewrite mulrC mulr2n mulrDl mul1r.
rewrite -lerBlDl.
apply le_trans with (`|h1 x - h2 x|).
- by apply lerB_dist.
- have ->: h1 x - h2 x = (h1 \- h2) x by [].
  rewrite -[`|h2 x|]mul1r.
  by apply HV.
Qed.



(* RESULT: Landau's sim is reflexive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_sim_reflexive
  {X: Type} {F: Filter X} {f: X -> ℝ}:
  f ∼_F f.
Proof.
rewrite /filter_real_Landau_sim.
have ->: f \- f = cst 0.
  apply functional_extensionality_dep.
  move=> x.
  rewrite /GRing.sub_fun /cst. (* F2 *)
  by rewrite subrr.
exists setT ; split=> //.
move=> x _.
rewrite /cst normr0.
apply mulr_ge0 => //.
by apply ltW.
Qed.



(* RESULT: Landau's sim is symmetric
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_sim_symmetric
  {X: Type} {F: Filter X} (f g: X -> ℝ):
  f ∼_F g -> g ∼_F f.
Proof.
move=> fsimg.
move=> η η_gt0. (* F4 ∼ ? =o ?*)
have: exists ϵ, [/\ 0 < ϵ, 0 < 1 - ϵ & ϵ / (1 - ϵ) <= η].
  pose ϵ := (Order.min (1 / 2) (η / 2)).
  have ϵ_gt0: 0 < ϵ.
    rewrite lt_min.
    by apply/andP ; split ; apply divr_gt0.
  have oneϵ_gt0: 0 < 1 - ϵ.
    rewrite subr_gt0.
    rewrite gt_min.
    apply/orP ; left.
    (* FIXME: shouldn't be that hard... *)
    rewrite [ltRHS](splitr 1).
    rewrite ltrDl.
    by rewrite mul1r invr_gt0.
  have ϵη_ok: ϵ / (1 - ϵ) <= η.
    rewrite ler_pdivrMr //.
    apply le_trans with (η / 2).
    - rewrite /ϵ ge_min.
      by apply/orP ; right.
    - rewrite -div1r ler_pM //.
      + by rewrite ltW.
      + by rewrite ltW // mul1r invr_gt0.
      + (* FIXME: too many lines for a trivial inequality *)
        rewrite [in leRHS](splitr 1).
        rewrite -addrA lerDl.
        rewrite subr_ge0.
        rewrite /ϵ ge_min.
        by apply/orP ; left.
  by exists ϵ.
move=> [ϵ [ϵ_gt0 oneϵ_gt0 ϵη_ok]].
have: {near F, fun x => `|(f \- g) x| <= ϵ * `|g x|}.
  by apply fsimg.
move=> [U [Unb H]]. (* F4 {near ...} *)
exists U ; split=> //.
move=> x xinU.
apply le_trans with (ϵ / (1 - ϵ) * `|f x|) ; last first.
  by rewrite ler_pM // ltW // divr_gt0.
rewrite mulrAC ler_pdivlMr //.
rewrite [leLHS]mulrC mulrDl mul1r.
rewrite mulNr lerBlDr.
rewrite -mulrDr.
apply le_trans with (ϵ * `|g x|).
- have ->: `|(g \- f) x| = `|(f \- g) x| by rewrite distrC.
  by apply H.
- rewrite ler_pM => // ; first by apply ltW.
  rewrite -lerBlDl.
  by rewrite lerB_dist.
Qed.



(* RESULT: Landau's sim is transitive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_sim_transitive
  {X: Type} (F: Filter X) (f g h: X -> ℝ):
  f ∼_F g -> g ∼_F h -> f ∼_F h.
Proof.
move=> fsimg gsimh.
rewrite /filter_real_Landau_sim.
have ->: f \- h = (f \- g) \+ (g \- h).
  apply functional_extensionality_dep.
  move=> x.
  rewrite /GRing.sub_fun /GRing.add_fun. (* F2 *)
  by ring.
apply filter_real_Landau_o_add => //.
by apply filter_real_Landau_sim_in_o with g.
Qed.



(* RESULT: Landau's sim and non-zero limit
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_sim_lim
  {X: Type} (F: Filter X) (f: X -> ℝ) (l: ℝ):
  l != 0 -> f ∼_F (cst l) <-> f @ F --> l%:E.
Proof.
move=> l_neq0.
split.
- move=> fsiml.
  have ->: l%:E = (0 + l%:E)%E by rewrite add0e.
  have ->: f = (fun x => (f x - l) + l).
    apply functional_extensionality_dep.
    move=> x.
    by ring.
  apply limit_real_shift.
  rewrite -filter_real_Landau_o1_zero_limit.
  have ->: (fun x => f x - l) = f \- (cst l).
    apply functional_extensionality_dep.
    move=> x.
    rewrite /GRing.sub_fun /cst. (* F2 *)
    by ring.
  apply filter_real_Landau_o_mute_constant with l => //.
  have ->: (fun x: X => l * cst 1 x) = cst l.
    apply functional_extensionality_dep.
    move=> x.
    by rewrite /cst mulr1.
  by [].
- move=> ftendstol.
  move: (@limit_real_shift _ _ _ _ (-l) ftendstol). (* FIXME: awful *)
  have ->: (l%:E + (- l)%:E)%E = 0%E by rewrite -EFinB subrr.
  have ->: (fun x: X => f x - l) = f \- cst l.
    apply functional_extensionality_dep.
    move=> x.
    by rewrite /GRing.sub_fun /cst. (* F2 *)
  rewrite -filter_real_Landau_o1_zero_limit.
  have invl_neq0: 1 / l != 0 by rewrite div1r invr_neq0.
  have ->: cst 1 = fun x: X => (1 / l) * (cst l x).
    apply functional_extensionality_dep.
    move=> x.
    by rewrite /cst mulrC mul1r divff.
    move=> almost.
  by apply filter_real_Landau_o_mute_constant with (1 / l).
Qed.
