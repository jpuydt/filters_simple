(* this file is about the theorems to compare limits (left, right, full) *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum order.
From mathcomp Require Import boolp classical_sets constructive_ereal mathcomp_extra reals.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.reals.definitions.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: neighbourhoods to the large left are neighbourhoods to strict left *)

Lemma filter_real_ltW: forall a V, V \is_nb (𝒩[≤] a) -> V \is_nb (𝒩[<] a).
Proof.
move=> a V.
move=> [n Hn]. (* F1 *)
exists n.
simpl. (* F1 *)
simpl in Hn. (* F1 *)
(* FIXME: ugly *)
apply subset_trans with (B:=([set x | `|x - a| <= 1 / 2 ^+ n] ∩ [set x | x <= a])) (C:=V) => //=.
apply setIS.
move=> x.
by apply ltW.
Qed.



(* RESULT: having a limit to the large left implies a limit to the strict left
   MC-A: TODO
   mathlib: TODO
*)
Lemma limit_real_ltW
  {X: Set} (F: Filter X)
  (f: ℝ -> X) (a: ℝ):
  f @ (𝒩[≤] a) --> F -> f @ 𝒩[<] a --> F.
Proof.
move=> f_tends_lt_a.
move=> W Wnb. (* F4 filter_tendsto *)
move: (f_tends_lt_a W Wnb) => [V [Vnb fVW]].
have Vnb_lt := (filter_real_ltW Vnb).
exists V.
by split.
Qed.



(* RESULT: neighbourhoods of a point are neighbourhoods to the large left
   MC-A: TODO
   mathlib: TODO
*)

Lemma filter_real_le_of_full: forall a V, V \is_nb a%:E -> V \is_nb (𝒩[≤] a).
Proof.
move=> a V.
move=> [n Hn].
simpl in Hn. (* F1 *)
exists n.
simpl. (* F1 *)
by apply subset_trans with [set x | `|x - a| <= 1 / 2 ^+ n].
Qed.



(* RESULT: having a limit implies a limit to the left
   MC-A: TODO
   mathlib: TODO
*)

Lemma limit_real_le_of_full
  {X: Set} (F: Filter X)
  (f: ℝ -> X) (a: ℝ):
  f @ a%:E --> F -> f @ 𝒩[≤] a --> F.
Proof.
move=> f_tends_a.
move=> W Wnb. (* F4 filter_tendsto *)
move: (f_tends_a W Wnb) => [V [Vnb fVW]].
exists V ; split=> //.
by apply (filter_real_le_of_full Vnb).
Qed.



(* RESULT: neighbourhoods to the large right are neighbourhoods to strict right *)

Lemma filter_real_gtW: forall a V, V \is_nb (𝒩[≥] a) -> V \is_nb (𝒩[>] a).
Proof.
move=> a V.
move=> [n Hn].
simpl in Hn. (* F1 *)
exists n => //=. (* F1 *)
apply subset_trans with (B:=([set x | `|x - a| <= 1 / 2 ^+ n] ∩ [set x | a <= x])) (C:=V) => //=.
apply setIS.
move=> x.
by apply ltW.
Qed.



(* RESULT: having a limit to the large right implies a limit to the strict right
   MC-A: TODO
   mathlib: TODO
*)
Lemma limit_real_gtW
  {X: Set} (F: Filter X)
  (f: ℝ -> X) (a: ℝ):
  f @ (𝒩[≥] a) --> F -> f @ 𝒩[>] a --> F.
Proof.
move=> f_tends_gt_a.
move=> W Wnb. (* F4 filter_tendsto *)
move: (f_tends_gt_a W Wnb) => [V [Vnb fVW]].
exists V ; split=> //.
by apply (filter_real_gtW Vnb).
Qed.



(* RESULT: neighbourhoods of a point are neighbourhoods to the large left
   MC-A: TODO
   mathlib: TODO
*)

Lemma filter_real_ge_of_full: forall a, forall V, V \is_nb a%:E -> V \is_nb (𝒩[≥] a).
Proof.
move=> a V.
move=> [n Hn].
simpl in Hn.   (* F1 *)
exists n.
simpl. (* F1 *)
by apply subset_trans with [set x | `|x - a| <= 1 / 2 ^+ n].
Qed.



(* RESULT: having a limit implies a limit to the right
   MC-A: TODO
   mathlib: TODO
*)

Lemma limit_real_ge_of_full
  {X: Set} (F: Filter X)
  (f: ℝ -> X) (a: ℝ):
  f @ a%:E --> F -> f @ 𝒩[≥] a --> F.
Proof.
move=> f_tends_a.
move=> W Wnb. (* F4 filter_tendsto *)
move: (f_tends_a W Wnb) => [V [Vnb fVW]].
exists V ; split=> //.
by apply (filter_real_ge_of_full Vnb).
Qed.



(* RESULT: compare a limit and left/right limits

   MC-A: TODO
   mathlib: TODO
 *)
Lemma limit_real_full_of_le_ge
  {X: Set} (F: Filter X)
  (f: ℝ -> X) (a: ℝ):
  f @ 𝒩[≤] a --> F -> f @ 𝒩[≥] a --> F -> f @ a%:E --> F.
Proof.
move=> f_tends_le_a f_tends_ge_a.
move=> W Wnb. (* F4 filter_tendsto *)
move: (f_tends_le_a W Wnb) => [U [Unb fUW]].
move: (f_tends_ge_a W Wnb) => [V [Vnb fVW]].
move: Unb => [m Hm].
simpl in Hm. (* F1 *)
move: Vnb => [n Hn].
simpl in Hn. (* F1 *)
apply filter_from_sequential_family_exists.
exists (m + n).
move=> y.
move=> [x xnba yfx].
case: (real_trisection x a).
- (* x = a *)
  move=> xa.
  rewrite -yfx.
  rewrite xa.
  apply fUW.
  rewrite -inE. (* F3 *)
  rewrite imagePin => //=.
  apply (in_superset Hm).
  rewrite inE => //=. (* F3 *)
  rewrite subrr normr0.
  split => //=.
  by rewrite div1r invr_ge0 exprn_ge0.
- (* x < a *)
  move=> xlta.
  rewrite -yfx.
  apply fUW.
  rewrite -inE. (* F3 *)
  rewrite imagePin => //=.
  apply (in_superset Hm).
  rewrite inE => //=. (* F3 *)
  split ; last by apply ltW.
  apply le_trans with (1 / 2 ^+ (m + n)); first by apply xnba.
  rewrite !div1r my_ler_pV2 ?exp2n_gt0 //.
  apply exp2n_nondecr.
  by rewrite leq_addr.
- (* a < x *)
  move=> altx.
  rewrite -yfx.
  apply fVW.
  rewrite -inE. (* F3 *)
  rewrite imagePin => //=.
  apply (in_superset Hn).
  rewrite inE => //=. (* F3 *)
  split ; last by apply ltW.
  apply le_trans with (1 / 2 ^+ (m + n)); first by apply xnba.
  rewrite !div1r my_ler_pV2 ?exp2n_gt0 //.
  apply exp2n_nondecr.
  by rewrite leq_addl.
Qed.



(* RESULT: a limit means a right/left limit

  MC-A: TODO
  mathlib: TODO
 *)
Lemma limit_real_to_ge_of_full
  {X: Set} (F: Filter X)
  (f: X -> ℝ) (a: ℝ):
  f @ F --> (𝒩[≥] a) -> f @ F --> a%:E.
Proof.
move=> f_tends_ge_a.
move=> W Wnb. (* F4 filter_tendsto *)
have Wnb_ge := filter_real_ge_of_full Wnb.
move: (f_tends_ge_a W Wnb_ge) => [V [Vnb fVW]]. (* F4 *)
by exists V.
Qed.
