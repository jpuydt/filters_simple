(* (* This file defines the various filters on the real numbers *) *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum order.
From mathcomp Require Import classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities.

Import Order.Theory GRing.Theory Num.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Open Scope ereal_scope.
Open Scope ring_scope.



(* DEFINITION: filter around an extended real point

   MC-A: ball_filter in topology.v and pinfty_nbhds and ninfty_nbhds in normedtype.v
   mathlib: TODO and at_top at_bottom in order/filter/at_top_bot.lean
*)
Section Real_filter.

Definition filter_real_item l n: set ℝ :=
match l with
| a%:E => [set x | `|x - a| <= (1 / 2 ^+ n)%R ]
| +oo => [set x | n%:R <= x]
| -oo => [set x | x + n%:R <= 0]
end.

Lemma filter_real_item_nonincr l: forall m n, (m <= n)%N -> filter_real_item l n ⊂ filter_real_item l m.
Proof.
rewrite -seq_subset_nonincreasing_iff_onestep.
move=> n.
case: l => [a| | ] ; rewrite /filter_real_item /mkset => x Hx. (* F1 *)
- apply le_trans with (1 / 2 ^+ n.+1) => //.
  rewrite !div1r my_ler_pV2 ?exp2n_gt0 //.
  by rewrite ltW // exp2n_incr.
- apply le_trans with n.+1%:R => //.
  by rewrite ler_nat.
- apply le_trans with (x + n.+1%:R) => //.
  by rewrite lerD2l ler_nat.
Qed.

Definition filter_real_sequential l := mkFilterSequentialFamily (@filter_real_item_nonincr l).
Definition filter_real l := filter_from_sequential_family (filter_real_sequential l).

#[warning="-uniform-inheritance"] Coercion filter_real: extended >-> Filter. (* FIXME: why this warning? *)

Lemma filter_real_contains_items (l: \bar ℝ): forall n, filter_real_item l n \is_nb l.
Proof.
move=> n.
by exists n.
Qed.

End Real_filter.

Arguments filter_real_item /.

#[global] Hint Resolve filter_real_contains_items: core.

Lemma filter_real_item_contains_point:
  forall l: ℝ, forall n, l ∈ filter_real_item l%:E n.
Proof.
move=> l n //=. (* F1 *)
rewrite inE /mkset. (* F3 *)
rewrite subrr normr0.
by rewrite div1r invr_ge0 exprn_ge0. (* FIXME: too much *)
Qed.

#[global] Hint Resolve filter_real_item_contains_point: core.

Lemma filter_real_finite_is_pointed:
  forall l, forall V, V \is_nb l%:E -> l ∈ V.
Proof.
move=> l.
move=> V Vnb.
move: Vnb => [n BnV]. (* F1 *)
simpl in BnV. (* F1 *)
apply (in_superset BnV).
by apply filter_real_item_contains_point. (* F1 this applies explicitly but not automatically *)
Qed.

Lemma filter_real_nontrivial (l: \bar ℝ): filter_nontrivial (filter_real l).
Proof.
case: l.
- (* l is real *)
  move=> l.
  move=> V Vnb.
  exists l.
  rewrite -inE. (* F3 *)
  by apply filter_real_finite_is_pointed.
- (* l is +oo *)
  apply filter_sequential_nontrivial.
  move=> N.
  exists N%:R.
  by rewrite //=. (* F2 *)
- (* l is -oo *)
  apply filter_sequential_nontrivial.
  move=> N.
  exists (-N%:R).
  rewrite //=. (* F1 *)
  have ->: (1 *- N + N%:R) = 0.
    move=> t.
    by rewrite addrC subrr.
  by [].
Qed.



(* DEFINITION: filters on strict left of real points

   MC-A: TODO
   mathlib: TODO
 *)

Section LessThanR.

Variable a: ℝ.

Definition filter_real_lt_item n: set ℝ := (filter_real_item a%:E n) ∩ [set x | x < a].

Lemma filter_real_lt_item_nonincr:
  forall m n, (m <= n)%N -> filter_real_lt_item n ⊂ filter_real_lt_item m.
Proof.
rewrite -seq_subset_nonincreasing_iff_onestep.
move=> n.
rewrite /filter_real_lt_item. (* F1 *)
apply setSI.
by apply filter_real_item_nonincr.
Qed.

Definition filter_real_lt_sequential := mkFilterSequentialFamily filter_real_lt_item_nonincr.

Definition filter_real_lt := filter_from_sequential_family filter_real_lt_sequential.

Lemma filter_real_lt_contains_items:
  forall n, seq_item filter_real_lt_sequential n \is_nb filter_real_lt.
Proof.
move=> n.
by exists n.
Qed.

Lemma filter_real_lt_nontrivial: filter_nontrivial filter_real_lt.
Proof.
apply filter_sequential_nontrivial.
move=> N.
exists (a - 1 / 2 ^+ N.+1) => //=.
rewrite //= /filter_real_lt_item //=.
split ; last first.
  rewrite gtrBl.
  rewrite div1r invr_gt0.
  by apply exp2n_gt0.
rewrite -addrAC subrr add0r normrN ger0_norm.
- rewrite !div1r my_ler_pV2 ?exp2n_gt0 //=.
  by rewrite ltW // exp2n_incr.
- rewrite ltW // div1r invr_gt0.
  by apply exp2n_gt0.
Qed.

End LessThanR.

Arguments filter_real_lt_item /.

#[global] Hint Resolve filter_real_lt_contains_items: core.

Reserved Notation "𝒩[<] a" (at level 70).
Notation "𝒩[<] a" := (filter_real_lt a).



(* DEFINITION: filters on large left of real points

   MC-A: TODO
   mathlib: TODO
 *)

Section LessEqualR.

Variable a: ℝ.

Definition filter_real_le_item n: set ℝ := (filter_real_item a%:E n) ∩ [set x | x <= a].

Lemma filter_real_le_item_nonincr:
  forall m n, (m <= n)%N -> filter_real_le_item n ⊂ filter_real_le_item m.
Proof.
rewrite -seq_subset_nonincreasing_iff_onestep.
move=> n.
rewrite /filter_real_le_item. (* F1 *)
apply setSI.
by apply filter_real_item_nonincr.
Qed.

Definition filter_real_le_sequential := mkFilterSequentialFamily filter_real_le_item_nonincr.

Definition filter_real_le := filter_from_sequential_family filter_real_le_sequential.

Lemma filter_real_le_contains_items:
  forall n, seq_item filter_real_le_sequential n \is_nb filter_real_le.
Proof.
move=> n.
by exists n.
Qed.

Lemma filter_real_le_nontrivial: filter_nontrivial filter_real_le.
Proof.
apply filter_sequential_nontrivial.
move=> N.
exists a => //=.
rewrite //= /filter_real_le_item //=.
split => //.
rewrite subrr normr0 !div1r invr_ge0 ltW //.
by apply exp2n_gt0.
Qed.

End LessEqualR.

Arguments filter_real_le_item /.

#[global] Hint Resolve filter_real_le_contains_items: core.

Reserved Notation "𝒩[≤] a" (at level 70).
Notation "𝒩[≤] a" := (filter_real_le a).


(* DEFINITION: filters on strict right of real points

   MC-A: TODO
   mathlib: TODO
 *)

Section GreaterThanR.

Variable a: ℝ.

Definition filter_real_gt_item n: set ℝ := (filter_real_item a%:E n) ∩ [set x | a < x].

Lemma filter_real_gt_item_nonincr:
  forall m n, (m <= n)%N -> filter_real_gt_item n ⊂ filter_real_gt_item m.
Proof.
rewrite -seq_subset_nonincreasing_iff_onestep.
move=> n.
rewrite /filter_real_gt_item. (* F1 *)
apply setSI.
by apply filter_real_item_nonincr.
Qed.

Definition filter_real_gt_sequential := mkFilterSequentialFamily filter_real_gt_item_nonincr.

Definition filter_real_gt := filter_from_sequential_family filter_real_gt_sequential.

Lemma filter_real_gt_contains_items: forall n, filter_real_gt_item n \is_nb filter_real_gt.
Proof.
move=> n.
by exists n.
Qed.

Lemma filter_real_gt_nontrivial: filter_nontrivial filter_real_gt.
Proof.
apply filter_sequential_nontrivial.
move=> N.
exists (a + 1 / 2 ^+ N.+1) => //=.
rewrite //= /filter_real_gt_item //=. (* F2 *)
split ; last first.
  rewrite ltrDl div1r invr_gt0.
  by apply exp2n_gt0.
rewrite addrAC subrr add0r ger0_norm.
- rewrite !div1r my_ler_pV2 ?exp2n_gt0 //=.
  by rewrite ltW // exp2n_incr.
- rewrite ltW // div1r invr_gt0.
  by apply exp2n_gt0.
Qed.

End GreaterThanR.

Arguments filter_real_gt_item /.

#[global] Hint Resolve filter_real_gt_contains_items: core.

Reserved Notation "𝒩[>] a" (at level 70).
Notation "𝒩[>] a" := (filter_real_gt a).



(* DEFINITION: filters on large right of real points

   MC-A: TODO
   mathlib: TODO
 *)

Section GreaterEqualR.

Variable a: ℝ.

Definition filter_real_ge_item n: set ℝ := (filter_real_item a%:E n) ∩ [set x | a <= x ].

Lemma filter_real_ge_item_nonincr:
  forall m n, (m <= n)%N -> filter_real_ge_item n ⊂ filter_real_ge_item m.
Proof.
rewrite -seq_subset_nonincreasing_iff_onestep.
move=> n.
rewrite /filter_real_ge_item. (* F1 *)
apply setSI.
by apply filter_real_item_nonincr.
Qed.

Definition filter_real_ge_sequential := mkFilterSequentialFamily filter_real_ge_item_nonincr.

Definition filter_real_ge := filter_from_sequential_family filter_real_ge_sequential.

Lemma filter_real_ge_contains_items: forall n, filter_real_ge_item n \is_nb filter_real_ge.
Proof.
move=> n.
by exists n.
Qed.

Lemma filter_real_ge_nontrivial: filter_nontrivial filter_real_ge.
Proof.
apply filter_sequential_nontrivial.
exists a => //=.
rewrite //= /filter_real_ge_item //=. (* F2 *)
split => //.
rewrite subrr normr0 !div1r invr_ge0 ltW //.
by apply exp2n_gt0.
Qed.

End GreaterEqualR.

Arguments filter_real_ge_item /.

#[global] Hint Resolve filter_real_ge_contains_items: core.

Reserved Notation "𝒩[≥] a" (at level 70).
Notation "𝒩[≥] a" := (filter_real_ge a).
