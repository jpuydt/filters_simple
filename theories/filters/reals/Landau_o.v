(* This file is about the Landau o notation *)

From mathcomp Require Import ssreflect ssrbool ssralg ssrfun ssrnum order eqtype.
From mathcomp.algebra_tactics Require Import ring.
From mathcomp Require Import classical_sets boolp functions constructive_ereal reals.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.nat filters.reals.definitions filters.reals.operations.opposite.
From Ici Require Import filters.reals.Landau_O sequences.reals.geometric.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: Landau o notation for real-valued functions *)

Definition filter_real_Landau_o
  {X: Type} (F: Filter X) (f g: X -> ℝ) :=
  forall (eps: ℝ), 0 < eps -> {near F, (fun x => `|f x| <= eps * `|g x|) }.

Reserved Notation "f '=o_' F g" (at level 70, no associativity, F at level 0, g at next level).
Notation "f '=o_' F g" := (filter_real_Landau_o F f g).

Reserved Notation "f '=' g '+o_' F h"
  (at level 70, no associativity, F at level 0, g at next level, h at next level).
Notation "f '=' g '+o_' F h" := (filter_real_Landau_o F (f \- g) h).



(* RESULT: multiplicative constants don't really impact Landau's o
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_mute_constant
  {X: Type} {F: Filter X} (f g: X -> ℝ):
  forall (C: ℝ), C != 0 -> f =o_F (fun x => C * g x) -> f =o_F g.
Proof.
move=> C C_neq0 foFg.
move=> eps eps_gt0. (* F4 =o *)
have epsC_gt0: 0 < eps / `|C|.
  apply divr_gt0 => //.
  by rewrite normr_gt0.
have: {near F, fun x => `|f x| <= eps / `|C| * `|C * g x|}.
  by apply foFg.
move=> [U [Unb fepsgU]]. (* F4 {near ...} *)
exists U ; split=> //.
move=> x xinU.
have ->: eps * `|g x| = eps / `|C| * `|C * g x|.
  (* FIXME: too many lines for a simple equality *)
  rewrite normrM -mulrA.
  rewrite [`|C| ^-1 * _]mulrA.
  rewrite [`|C| ^-1 * _]mulrC.
  rewrite divff ; last first.
    apply lt0r_neq0.
    by rewrite normr_gt0.
  by rewrite mul1r.
by apply fepsgU.
Qed.



(* RESULT: Landau's o is transitive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_trans
  {X: Type} {F: Filter X} (f g h: X -> ℝ):
  f =o_F g -> g =o_F h -> f=o_F h.
Proof.
move=> foFg goFh.
move=> eps eps_gt0.
have: {near F, fun x => `|f x| <= eps * `|g x|}.
  by apply foFg.
move=> [U [Unb fepsgU]]. (* F4 {near ...} *)
have: {near F, fun x => `|g x| <= 1 *`|h x|}.
  by apply goFh.
move=> [V [Vnb ghV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (eps * `|g x|).
- by apply fepsgU.
- apply ler_pM => // ; first by rewrite ltW.
  rewrite -[`|h x|]mul1r.
  by apply ghV.
Qed.



(* RESULT: Landau's O and o are transitive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_oO_trans
  {X: Type} {F: Filter X} (f g h: X -> ℝ):
  f =o_F g -> g =O_F h -> f=o_F h.
Proof.
move=> foFg.
move=> [M [M_gt0 [U [Unb gMhU]]]]. (* F4 =O and {near ...} *)
move=> eps eps_gt0. (* F4 =o *)
have epsM_gt0: 0 < eps / M by apply divr_gt0.
have: {near F, fun x => `|f x| <= eps / M * `|g x|}.
  by apply foFg.
move=> [V [Vnb fepsMgV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (eps / M * `|g x|) => //.
- by apply fepsMgV.
- rewrite -mulrA.
  apply ler_pM => // ; first by rewrite ltW.
  + apply mulr_ge0 => //.
    by rewrite invr_ge0 ltW.
  + rewrite ler_pdivrMl //.
    by apply gMhU.
Qed.



(* RESULT: Landau's O and o are transitive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_Oo_trans
  {X: Type} {F: Filter X} (f g h: X -> ℝ):
  f =O_F g -> g =o_F h -> f=o_F h.
Proof.
move=> [M [M_gt0 [U [Unb fMgU]]]]. (* F4 =O and {near ...} *)
move=> goFh.
move=> eps eps_gt0. (* F4 =o *)
have epsM_gt0: 0 < eps / M by apply divr_gt0.
have: {near F, fun x => `|g x| <= eps / M * `|h x|}.
  by apply goFh.
move=> [V [Vnb gepsMhV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (M * `|g x|) ; first by apply fMgU.
rewrite mulrC -ler_pdivlMr //.
rewrite mulrAC.
by apply gepsMhV.
Qed.



(* RESULT: a Landau o is a O
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_is_O
  {X: Type} {F: Filter X} (f g: X -> ℝ):
  f =o_F g -> f =O_F g.
Proof.
move=> foFg.
have: {near F, fun x => `|f x| <= 1 * `|g x|}.
  by apply foFg.
move=> [U [Unb fgU]]. (* F4 {near ...} *)
exists 1 ; split=> //.
by exists U.
Qed.



(* RESULT: Landau's o is additive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_add
  {X: Type} {F: Filter X} (f g h: X -> ℝ):
  f =o_F h -> g =o_F h -> f \+ g =o_F h.
Proof.
move=> foFh goFh.
move=> eps eps_gt0.
have eps2_gt0: 0 < eps / 2 by apply divr_gt0.
have: {near F, fun x => `|f x| <= eps / 2 * `|h x|}.
  by apply foFh.
move=> [U [Unb feps2hU]]. (* F4 {near ...} *)
have: {near F, fun x => `|g x| <= eps / 2 * `|h x|}.
  by apply goFh.
move=> [V [Vnb geps2hV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (`|f x| + `|g x|) ; first by apply ler_normD.
rewrite -[eps]mulr1 [1]splitr. (* FIXME: inelegant *)
rewrite mulrDr mulrDl div1r.
apply lerD.
- by apply feps2hU.
- by apply geps2hV.
Qed.



(* RESULT: Landau's o is additive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_add2
  {X: Type} {F: Filter X} (f1 g1 f2 g2 h: X -> ℝ):
  f1 = g1 +o_F h -> f2 = g2 +o_F h -> f1 \+ f2 = g1 \+ g2 +o_F h.
Proof.
move=> f1g1oFh f2g2oFh.
have ->: (f1 \+ f2) \- (g1 \+ g2) = (f1 \- g1) \+ (f2 \- g2). (* FIXME: sigh... *)
  apply functional_extensionality_dep.
  move=> x.
  rewrite /GRing.add_fun /GRing.sub_fun. (* F2 *)
  by ring.
by apply filter_real_Landau_o_add.
Qed.



(* RESULT: Landau's o is stable by composition
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_comp1
  {X: Type} {F: Filter X}
  {Y: Type} (G: Filter Y)
  (f: X -> Y) (g h: Y -> ℝ):
  g =o_G h -> f @ F --> G -> (g \o f) =o_F (h \o f).
Proof.
move=> gohG fFG.
move=> eps eps_gt0.
move: (gohG eps eps_gt0).
move=> [V [Vnb gepshV]]. (* F4 {near ...} *)
move: (fFG V Vnb).
move=> [U [Unb fUV]]. (* F4 *)
exists U ; split=> //.
move=> x xinU.
apply gepshV.
in_superset fUV.
by apply imagePin.
Qed.



(* RESULT: Landau's o is stable by composition
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o_comp2
  {X: Type} {F: Filter X}
  {Y: Type} {G: Filter Y}
  (f: X -> Y) (g h k: Y -> ℝ):
  g = h +o_G k -> f @ F --> G -> (g \o f) = (h \o f) +o_F (k \o f).
Proof.
move=> gohG fFG.
have ->: (g \o f) \- (h \o f) = (g \- h) \o f by apply functional_extensionality_dep.
by apply filter_real_Landau_o_comp1 with G.
Qed.



(* RESULT: Landau o(1) is 0-limit
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_o1_zero_limit
  {X: Type} {F: Filter X}
  (f: X -> ℝ):
  f =o_F (cst 1) <-> f @ F --> 0%:E.
Proof.
split.
- move=> fo1.
  rewrite -filter_tendsto_to_sequential.
  move=> n.
  have e2n_gt0: 0 < 1 / 2 ^+ n :> ℝ by rewrite mul1r invr_gt0 exprn_gt0.
  move: (fo1 (1 / 2 ^+ n) e2n_gt0). (* F1 *)
  rewrite /cst normr1 mulr1.
  move=> [U [Unb HU]]. (* F4 {near ...} *)
  exists U ; split=> //.
  rewrite image_sub /preimage.
  move=> x.
  rewrite -inE /mkset => xinU.
  simpl.
  rewrite subr0.
  by apply HU.
- move=> fF0.
  move=> ϵ ϵ_gt0.
  have: exists n, 1 / 2 ^+ n < ϵ.
    by apply exp2n_epsilonesque.
  move=> [n Hn].
  move: (fF0 (filter_real_item 0 n) (filter_real_contains_items 0 n)). (* F1 *)
  move=> [U [Unb fUBn]]. (* F4 [locally ...] *)
  exists U ; split=> //.
  move=> x xinU.
  rewrite /cst normr1 mulr1.
  apply ltW.
  apply le_lt_trans with (1 / 2 ^+ n) => //.
  rewrite -[f x]subr0.
  apply fUBn.
  rewrite -inE.
  by rewrite imagePin.
Qed.
