(* This file is about inequalities and real filters *)

From mathcomp Require Import ssreflect ssrbool ssrnat ssralg ssrnum order archimedean.
From mathcomp Require Import boolp classical_sets constructive_ereal mathcomp_extra reals.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.nat filters.reals.definitions filters.reals.operations.opposite.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: real comparison gives a result stronger than mere separation *)

Lemma filter_real_order:
  forall (a b: \bar ℝ), (a < b)%E
 ->  exists Va Vb, [/\ Va \is_nb a,
                       Vb \is_nb b
                     & forall (x y: ℝ), x ∈ Va -> y ∈ Vb -> x < y].
Proof.
(* serious part of the proof: by case on the values of a and b ;
   that will make 3x3 cases, some trivial and some painful *)
case => [ a | // | // ].
- case => [ b | // | // ].
  + (* here both a and b are finite: painful case *)
    move=> a_lt_b.
    rewrite lte_fin -subr_gt0 in a_lt_b.
    pose n := Num.bound ((b-a)^-1).
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists n.+1.
    exists n.+1.
    move=> x y.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    move=> /ler_distlDr x_le_aplus.
    rewrite distrC.
    move=> /ler_distlBl y_ge_bminus.
    apply le_lt_trans with (a + 1 / 2 ^+ n.+1) => //.
    apply lt_le_trans with (b - 1 / 2 ^+ n.+1) => //.
    rewrite ltrBDr.
    rewrite -addrA -ltrBDl.
    have -> : 1 / 2 ^+ n.+1 + 1 / 2 ^+ n.+1 = 1 / 2 ^+ n :> ℝ.
      (* FIXME: how many ugly lines for a trivial equality?! *)
      apply (@mulfI _ (2 ^+ (n.+1) : ℝ)) ; first by rewrite lt0r_neq0 // exprn_gt0 // ltr0n.
      rewrite mulrDr !mul1r divff ; last by rewrite lt0r_neq0 // exprn_gt0 // ltr0n.
      rewrite -expfB_cond ; first last. (* FIXME bool + ℕ ... *)
        by rewrite (negPf (lt0r_neq0 _)) //=. (* FIXME: dear... *)
      by rewrite subSnn.
    rewrite -my_ltr_pV2 ?inE // ; last by rewrite div1r invr_gt0 exprn_gt0 // ltr0n.
    rewrite div1r invrK.
    apply lt_trans with n%:R ; last by rewrite -natrX ltr_nat n_lt_exp2n.
    by rewrite /n archi_boundP // ltW // invr_gt0.
  + (* here a is finite and b = +oo so it's a relatively trivial case *)
    move=> _.
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists 0%N.
    exists (Num.bound (`|a| + 1)).
    move=> x y.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite expr0 divr1.
    move=> /ler_distlDr x_le_aplus.
    move=> y_ge_bminus.
    apply le_lt_trans with (a + 1) => //.
    apply lt_le_trans with (Num.bound (`|a| + 1))%:R => //.
    apply le_lt_trans with (`|a| + 1) ; first by rewrite lerD2r ler_norm.
    by rewrite archi_boundP // addr_ge0.
- (* here a = +oo, so comparison with b will be false and we're done *)
  by case.
- (* here a = -oo *)
  case => [ b | // | //].
  + (* a = -oo and b is finite *)
    move=> _. (* -oo < b is uninteresting *)
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists (Num.bound `|-b| + 1)%N.
    exists 0%N.
    move=> x y.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite addnC natrD addrA.
    rewrite -lerBDr sub0r => x_le_minusb1.
    rewrite expr0 divr1 //.
    rewrite distrC.
    move=> /ler_distlBl bminus_le_y.
    apply lt_le_trans with (b - 1) => //.
    rewrite ltrBDr.
    apply le_lt_trans with (1 *- Num.bound `|-b|) => //. (* FIXME: ugly *)
    rewrite ltrNl.
    apply le_lt_trans with `|-b|; first by apply ler_norm.
    by apply archi_boundP.
  + (* here a = -oo and b = +oo: a simple case *)
    move=> _. (* -oo < +oo is uninteresting *)
    apply filter_from_sequential_family_exists2. (* F1 *)
    exists 0%N.
    exists 1%N.
    move=> x y.
    simpl. (* F1 *)
    rewrite !inE /mkset. (* F3 *)
    rewrite addr0 => x_le0.
    move=> y_ge1.
    apply le_lt_trans with 0 => //.
    by apply lt_le_trans with 1.
Qed.



(* RESULT: this is the unicity of (extended) real limits *)

Corollary filter_real_is_separated: filter_family_is_separated filter_real.
Proof.
move=> l1 l2.
wlog l1ltl2: l1 l2 / (l1 < l2)%E => [th_asym|]. (* FIXME: unreadable! *)
  move=> l1neql2.
  case: (le_total l1 l2) => /orP [l1lel2 | l2lel1]. (* FIXME: warning about a spurious ssr injection from le_total *)
    apply th_asym => //.
    rewrite lt_def.
    apply/andP ; split => //.
    by rewrite eqtype.eq_sym. (* FIXME: why the need for eqtype. ? *)
  have l2ltl1: (l2 < l1)%E by rewrite lt_def l1neql2 l2lel1.
  have: exists V2 V1, [/\ V2 \is_nb l2, V1 \is_nb l1 & (V2 ∩ V1) = set0].
    rewrite eqtype.eq_sym in l1neql2. (* FIXME: why the need for eqtype. ? *)
    by apply th_asym.
  move=> [V2 [V1 [V2nb V1nb disjV1V2]]].
  rewrite setIC in disjV1V2.
  exists V1.
  exists V2.
  by split.
move=> l1neql2.
have: exists V1 V2, [/\ V1 \is_nb l1, V2 \is_nb l2 & forall x y: ℝ, x ∈ V1 -> y ∈ V2 -> x < y].
  by apply filter_real_order.
move=> [V1 [V2 [V1nb V2nb V1ltV2]]].
exists V1.
exists V2.
split=> //.
apply: disj_set2P. (* FIXME: here 'apply:' works and not 'apply' *)
apply /disj_setPS.
move=> x.
rewrite -in_setE in_setI => /andP [V1x V2x].
exfalso ; rewrite -falseE. (* FIXME: inelegant *)
rewrite -(ltxx x).
by apply V1ltV2.
Qed.

#[global] Hint Resolve filter_real_is_separated: core.



(* RESULT: limits in inequalities
   MC-A: ler_cvg_to in normedtype.v
   mathlib: le_of_tendsto_of_tendsto in topology/algebra/order/basic.lean
 *)

Lemma limit_real_le
  (X: Type) (F: Filter X)
  (f g: X -> ℝ) (lf lg: \bar ℝ):
  filter_nontrivial F
  -> f @ F --> lf
  -> g @ F --> lg
  -> {near F, fun a => f a <= g a}
  -> (lf <= lg)%E.
Proof.
move=> Fnontrivial f_tends_to_lf g_tends_to_lg.
move=> [Uleq [Uleqnb fleqg]]. (* F4 {near ...} *)
rewrite comparable_leNgt ; last by apply: comparableT.
apply/negP => lgltlf.
(* from filtered_real_order, we get two neighbourhoords Vf and Vg with Vg < Vf in some sense,
   so from the limits we get two neighbourhoods Uf and Ug at the source arriving in them
 *)
move: (filter_real_order lgltlf).
move=> [Vg [Vf [Vgnb Vfnb VgltVf]]].
move: (f_tends_to_lf Vf Vfnb)=> [Uf [Ufnb fUfVf]]. (* F4 [locally ...] *)
move: (g_tends_to_lg Vg Vgnb)=> [Ug [Ugnb gUgVg]]. (* F4 [locally ...] *)

(* let's turn these three neighbourhoods into one! *)
pose U := Uleq ∩ (Uf ∩ Ug).
have Unb: U \is_nb F by rewrite /U.

(* now we're going to find an x in U and prove it has g x < g x *)
have: U !=set0 by apply Fnontrivial.

move=> [x]. (* F4 getting an element in a non-empty set *)
rewrite -inE. (* F3 *)
rewrite in_setI => /andP [xinUleq]. (* F4 getting an element in a three-set *)
rewrite in_setI => /andP [xinUf xinUg]. (* F4 intersection is too hard *)
rewrite -falseE -(ltxx (g x)). (* FIXME: inelegant *)
have fxVf: f x ∈ Vf.
  in_superset fUfVf.
  by apply imagePin.
have gxVg: g x ∈ Vg.
  in_superset gUgVg.
  by apply imagePin.
have gxltfx : g x < f x.
  by rewrite VgltVf.
have fxgegx : f x <= g x.
  by rewrite fleqg.
by apply lt_le_trans with (f x).
Qed.



(* RESULT: +oo limit by minoration
   MC-A: ger_cvgy in normedtype.v
   mathlib: tendsto_at_top_mono in order/filter/at_top_bot.lean
 *)

Lemma limit_real_pinfty_by_minoration
  (X: Type) (F: Filter X)
  (f g: X -> ℝ):
  f @ F --> +oo
  -> {near F, fun x => f x <= g x}
  -> g @ F --> +oo.
Proof.
move=> f_tends_to_pinfty.
move=> [U [Unb fleg]]. (* F4 {near ...} *)
rewrite -filter_tendsto_to_sequential.
move=> n.
have: [locally F, fun V => [set f x | x in V] ⊂ seq_item (filter_real_sequential +oo) n]. (* FIXME: ugly *)
  apply f_tends_to_pinfty.
  by rewrite /seq_item.
move=> [V [Vnb fVgen]]. (* F4 [locally ...] *)
exists (U ∩ V) ; split=> //.
(* now things are in place to start focusing on the real goal *)
rewrite !image_sub /preimage /seq_item //= in fVgen. (* F1 and F3 ? *)
move=> y.
move=> [x [xinU xinV] y_eq_gx].
simpl. (* F1 *)
rewrite -y_eq_gx.
apply le_trans with (f x).
- by apply (fVgen x xinV).
- by apply fleg ; rewrite inE. (* F3 *)
Qed.



(* RESULT: -oo limit by majoration
   MC-A: ler_cvgNy in normedtype.v
   mathlib: tendsto_at_top_mono in order/filter/at_top_bot.lean
 *)

Lemma limit_real_minfty_by_majoration
  (X: Type) (F: Filter X)
  (f g: X -> ℝ):
  g @ F --> -oo
  -> {near F, fun x => f x <= g x}
  -> f @ F --> -oo.
Proof.
move=> g_tends_to_minfty.
move=> [U [FU flegU]]. (* F4 {near ...} *)
have ->: -oo = oppe +oo :> \bar ℝ by rewrite /oppe.
have ->: f = \- \- f.
  (* FIXME: very ugly... *)
  apply functional_extensionality_dep.
  move=> x.
  unfold GRing.opp_fun. (* F2 *)
  by rewrite opprK.
apply limit_real_opp.
apply limit_real_pinfty_by_minoration with (\- g).
- have ->: +oo = oppe (-oo) :> \bar ℝ by rewrite /oppe.
  by apply limit_real_opp.
- exists U ; split=> //.
  move => x Ux.
  rewrite lerN2.
  by apply flegU.
Qed.


(* RESULT: squeeze theorem
   MC-A: squeeze_cvgr in normedtype.v
   mathlib: tendsto_of_tendsto_of_tendsto_of_le_of_le in topology/algebra/order/basic.lean
            (but uses tendsto.of_small_sets of order/filter/small_sets.lean, much harder)
 *)

Theorem limit_real_squeeze
  (X: Type) (F: Filter X)
  (l: ℝ) (f g h: X -> ℝ):
  f @ F --> l%:E
  -> h @ F --> l%:E
  -> {near F, fun a => f a <= g a <= h a}
  -> g @ F --> l%:E.
Proof.
move=> f_tends_to_l h_tends_to_l.
move=> [U [Unb fleglehU]]. (* F4 {near ...} *)
have flegU: forall x, x ∈ U -> f x <= g x.
  move=> x xinU.
  have: f x <= g x <= h x by apply fleglehU.
  by move/andP => [ // ]. (* FIXME: a bit silly *)
have glehU: forall x, x ∈ U -> g x <= h x.
  move=> x xinU.
  have: f x <= g x <= h x by apply fleglehU.
  by move/andP => [ // ]. (* FIXME: a bit silly *)
rewrite -filter_tendsto_to_sequential.
move=> n.

(* both f and h get to the ball on some neighbourhood, each its own *)

have: [locally F, fun V => [set f x | x in V] ⊂ filter_real_item l%:E n].
  by apply f_tends_to_l.
move=> [Vf [Vfnb Vf_n]]. (* F4 [locally ...] but incomplete because we need a simpl *)
move: Vf_n.
simpl. (* F1 *)
rewrite image_sub /preimage => Vf_n.
have: [locally F, fun V => [set h x | x in V] ⊂ filter_real_item l%:E n].
  by apply h_tends_to_l.
move=> [Vh [Vhnb Vh_n]]. (* F4 [locally ...] *)
move: Vh_n.
simpl. (* F1 *)
rewrite image_sub /preimage => Vh_n.


(* so the intersection of U, Vf and Vh will get a good neighbourhood for everything *)

exists (U ∩ (Vf ∩ Vh)) ; split=> //.
rewrite image_sub /preimage.
move=> x [xinU [xinVf xinVh]].
rewrite -inE in xinU. (* F3 *)
rewrite -inE in xinVf. (* F3 *)
rewrite -inE in xinVh. (* F3 *)
simpl.
rewrite ler_distl.
apply/andP ; split.
- apply le_trans with (f x) ; last by apply flegU.
  rewrite ler_distlBl // distrC.
  apply Vf_n.
  by rewrite -inE. (* F3 *)
- apply le_trans with (h x) ; first by apply glehU.
  rewrite ler_distlDr //.
  apply Vh_n.
  by rewrite -inE. (* F3 *)
Qed.



(* RESULT: an upper bound is a limit point
   MC-A: TODO
   mathlib: TODO

  FIXME: where is sup: set ℝ -> \bar ℝ ?
 *)

Lemma filter_real_upper_bound:
  forall (A: set ℝ), has_sup A
  -> forall W, W \is_nb (sup A)%:E -> exists w, w ∈ (A ∩ W)
                                                  /\ forall a, a ∈ A -> w <= a -> a ∈ W.
Proof.
move=> A has_supA.
apply filter_from_sequential_family_local_prop. (* F1 *)
- move=> U V Unb Vnb UsubV. (* F4 filter_prop_local *)
  move=> [w [wAU HAV]].
  move: wAU; rewrite in_setI => /andP [Aw Uw]. (* FIXME: ugly *)
  exists w ; split=> //.
    rewrite in_setI ; apply/andP ; split => //.
    by in_superset UsubV.
  move=> a Aa wlea.
  in_superset UsubV.
  by apply HAV.
- move=> n.
  pose ϵ : ℝ := 1 / 2 ^+ n.
  have ϵ_gt0: 0 < ϵ by rewrite /ϵ div1r invr_gt0 exprn_gt0.
  move: (sup_adherent ϵ_gt0 has_supA) => [w Aw w_gt]. (* FIXME: ugly *)
  rewrite -inE in Aw. (* F3 *)
  exists w ; split=> //.
  + rewrite in_setI ; apply/andP ; split => //.
    simpl. (* F1 *)
    rewrite inE /mkset. (* F3 *)
    rewrite -/ϵ ler_norml.
    apply/andP ; split ; first by rewrite lerBDl ltW.
    rewrite lerBDl.
    apply le_trans with (sup A).
    -- apply sup_upper_bound => //.
       by rewrite -inE. (* F3 *)
    -- by rewrite ltW // ltrDl.
  + move=> a Aa w_le_a.
    simpl. (* F1 *)
    rewrite inE /mkset. (* F3 *)
    rewrite -/ϵ ler_norml.
    apply/andP ; split.
    -- rewrite lerBDl ltW //.
       by apply lt_le_trans with w.
    -- rewrite lerBDr.
       rewrite addrC -[a]addr0.
       apply lerD ; last by rewrite ltW.
       apply sup_upper_bound => //.
       by rewrite -inE. (* F3 *)
Qed.



(* RESULT: zero limit by domination
   MC-A: TODO
   mathlib: TODO
 *)

Lemma limit_real_zero_by_domination
  (X: Type) (F: Filter X)
  (f g: X -> ℝ):
  g @ F --> 0%:E
  -> {near F, fun a => `|f a| <= g a}
  -> f @ F --> 0%:E.
Proof.
move=> g_tendsto_zero.
move=> [U [Unb gfU]]. (* F4 {near ...} *)
apply limit_real_squeeze with (\- g) g => //.
- have ->: 0%:E = (-0)%E by move=> t ; rewrite -oppr0.  (* FIXME: so ugly... *)
  by apply limit_real_opp.
- exists U ; split=> //.
  move=> x xinU.
  rewrite -ler_norml.
  by apply gfU.
Qed.



(* RESULT: a positive real has a positive neighbourhood
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_gt0_near_gt0 (l: ℝ):
 0 < l -> {near l%:E, fun x => 0 < x}.
Proof.
move=> l_gt0.
have: exists V0 Vl, [/\ V0 \is_nb 0%E, Vl \is_nb l%:E & forall x y, x ∈ V0 -> y ∈ Vl -> x < y].
  by apply filter_real_order.
move=> [V0 [Vl [V0nb Vlnb V0ltVl]]].
exists Vl ; split=> //.
move=> x Vlx.
apply V0ltVl => //.
by apply filter_real_finite_is_pointed.
Qed.



(* RESULT: positive limit implies positive near
   MC-A: TODO
   mathlib: TODO
 *)

Lemma limit_real_gt0_near_gt0
 (X: Type) (F: Filter X)
 (f: X -> ℝ) (l: ℝ):
 f @ F --> l%:E -> 0 < l -> {near F, fun x => 0 < f x}.
Proof.
move=> f_tendsto_l l_gt0.
have: {near l%:E, fun x => 0 < x}.
  by apply filter_real_gt0_near_gt0.
move=> [V [Vnb V_gt0]]. (* F4 {near ...} *)
have: [locally F, fun A => [set f x | x in A] ⊂ V].
  by apply f_tendsto_l.
move=> [A [Anb fAV]]. (* F4 [locally ...] *)
move: fAV.
rewrite image_sub /preimage.
move=> fAV.
exists A ; split=> //.
move=> a Aa.
apply V_gt0.
rewrite inE. (* F3 *)
apply fAV.
by rewrite -inE. (* F3 *)
Qed.
