(* This file is about the Landau O notation *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrfun ssrnum order.
From mathcomp.algebra_tactics Require Import ring.
From mathcomp Require Import boolp classical_sets constructive_ereal mathcomp_extra reals.

From Ici Require Import missing filters.generalities.
From Ici Require Import filters.nat filters.reals.definitions filters.reals.operations.opposite.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: Landau O notation for real-valued functions *)

Definition filter_real_Landau_O
  {X: Type} (F: Filter X) (f g: X -> ℝ) :=
  exists (M: ℝ), 0 < M /\ {near F, (fun x => `|f x| <= M * `|g x|) }.

Reserved Notation "f '=O_' F g" (at level 70, no associativity, F at level 0, g at next level).
Notation "f '=O_' F g" := (filter_real_Landau_O F f g).

Reserved Notation "f '=' g '+O_' F h"
  (at level 70, no associativity, F at level 0, g at next level, h at next level).
Notation "f '=' g '+O_' F h" := (filter_real_Landau_O F (f \- g) h).



(* RESULT: Landau's O is reflexive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_O_reflexive
  {X: Type} {F: Filter X} (f: X -> ℝ):
  f =O_F f.
Proof.
exists 1 ; split=> //. (* F4 =O *)
exists setT ; split=> //. (* F4 {near ...} *)
move=> x _.
by rewrite mul1r.
Qed.



(* RESULT: multiplicative constants don't really impact Landau's O
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_O_mute_constant
  {X: Type} {F: Filter X} (f g: X -> ℝ):
  forall (C: ℝ), 0 < C -> f =O_F g -> f =O_F (fun x => C * g x).
Proof.
move=> C Cgt0.
move=> [M [M_gt0 [U [Unb fMgU]]]]. (* F4 =O and a {near ...} *)
exists (M / C).
split; first by apply divr_gt0.
exists U; split=> //.
move=> x xinU.
rewrite normrM.
have Cge0: 0 <= C by rewrite ltW.
rewrite (normr_idP Cge0). (* FIXME: didn't manage to apply or rewrite normr_idP... *)
rewrite [C*_]mulrC mulrACA.
rewrite mulVf ; last by apply lt0r_neq0.
rewrite mulr1.
by apply fMgU.
Qed.



(* RESULT: Landau's O is transitive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_O_trans
  {X: Type} {F: Filter X} (f g h: X -> ℝ):
  f =O_F g -> g =O_F h -> f=O_F h.
Proof.
move=> [M [M_gt0 [U [Unb fMgU]]]]. (* F4 =O and {near ...} *)
move=> [N [N_gt0 [V [Vnb gNhV]]]]. (* F4 =O and {near ...} *)
exists (M * N).
split ; first by apply mulr_gt0.
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (M * `|g x|).
- by apply fMgU.
- rewrite -mulrA.
  apply ler_pM => //.
  + by apply ltW.
  + by apply gNhV.
Qed.



(* RESULT: Landau's O is additive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_O_add
  {X: Type} {F: Filter X} (f g h: X -> ℝ):
  f =O_F h -> g =O_F h -> f \+ g =O_F h.
Proof.
move=> [M [M_gt0 [U [Unb fMhU]]]]. (* F4 =O *)
move=> [N [N_gt0 [V [Vnb gNhV]]]]. (* F4 =O *)
exists (M + N).
split ; first by apply addr_gt0.
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply le_trans with (`|f x| + `|g x|) ; first by apply ler_normD.
rewrite mulrDl.
apply lerD.
- by apply fMhU.
- by apply gNhV.
Qed.



(* RESULT: Landau's O is additive
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Landau_O_add2
  {X: Type} {F: Filter X} (f1 g1 f2 g2 h: X -> ℝ):
  f1 = g1 +O_F h -> f2 = g2 +O_F h -> f1 \+ f2 = g1 \+ g2 +O_F h.
Proof.
move=> f1g1OFh f2g2OFh.
have ->: (f1 \+ f2) \- (g1 \+ g2) = (f1 \- g1) \+ (f2 \- g2).
  apply functional_extensionality_dep.
  move=> x.
  rewrite /GRing.add_fun /GRing.sub_fun. (* F2 *)
  by ring.
by apply filter_real_Landau_O_add.
Qed.



(* RESULT: Landau's O is stable by composition
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Lando_O_comp1
  {X: Type} {F: Filter X}
  {Y: Type} (G: Filter Y)
  (f: X -> Y) (g h: Y -> ℝ):
  g =O_G h -> f @ F --> G -> (g \o f) =O_F (h \o f).
Proof.
move=> [M [M_gt0 [V [Vnb gMhV]]]]. (* F4 =O and {near ...} *)
move=> fFG.
exists M.
split => //.
have: [locally F, fun U => [set f x | x in U] ⊂ V].
  by apply fFG.
move=> [U [Unb fUV]]. (* F4 [locally ...] *)
exists U ; split=> //.
move=> x xinU.
apply gMhV.
in_superset fUV.
by apply imagePin.
Qed.



(* RESULT: Landau's O is stable by composition
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_real_Lando_O_comp2
  {X: Type} {F: Filter X}
  {Y: Type} {G: Filter Y}
  (f: X -> Y) (g h k: Y -> ℝ):
  g = h +O_G k -> f @ F --> G -> (g \o f) = (h \o f) +O_F (k \o f).
Proof.
move=> gOhG fFG.
have ->:  (g \o f) \- (h \o f) = (g \- h) \o f by apply functional_extensionality_dep.
by apply filter_real_Lando_O_comp1 with G.
Qed.
