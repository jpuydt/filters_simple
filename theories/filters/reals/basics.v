(* this file contains basic results on real filters *)

From mathcomp Require Import ssreflect ssralg ssrnum.
From mathcomp Require Import functions constructive_ereal reals.

From Ici Require Import missing filters.generalities filters.sequential filters.nat.
From Ici Require Import filters.reals.definitions.

Import Num.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* RESULT: a constant function has a limit
   MC-A: cvg_cst in topology.v
   mathlib: tendsto_const_nhds in topology/basic.lean
 *)

Lemma limit_real_constant
  {X: Type} {F: Filter X} {l: ℝ}: (cst l) @ F --> l%:E.
Proof.
apply filter_tends_to_constant.
by apply filter_real_finite_is_pointed.
Qed.



(* RESULT: integers go infinity
   MC-A: TODO
   mathlib: TODO
 *)

Lemma limit_real_nat: GRing.natmul 1 @ ∞ --> +oo%E.
Proof.
apply filter_tendsto_to_sequential.
move=> n.
apply filter_from_sequential_family_exists. (* F1 *)
exists n.
move=> x [m n_le_m x_is_m] //=. (* F1 *)
rewrite -x_is_m.
by rewrite ler_nat.
Qed.
