(* This file contains the definition of the filter for uniform convergence of real-valued functions *)


From mathcomp Require Import ssreflect ssrbool ssrnat ssrfun.
From mathcomp Require Import classical_sets boolp.

From Ici Require Import missing filters.generalities.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Section UniformConvergence.

(* Variable X: Set. *)

(* Variable l: X -> ℝ. (* FIXME: how do I consider the set of functions as a set itself? *) *)

(* Definition filter_uniform_convergence_item (n: ℕ): set (X -> ℝ) := *)
(*   [set f | forall t, `|f(t) - l(t)| <= (1 / 2 ^+ n)%R ]. *)

(* Lemma filter_uniform_convergence_item_nonempty: *)
(*   forall n, filter_uniform_convergence_item n !=set0. *)
(* Proof. *)
(*   move=> n. *)
(*   exists l. *)
(*   move=> t. *)
(*   rewrite subrr etc. *)
(* Qed. *)
  
End UniformConvergence.
