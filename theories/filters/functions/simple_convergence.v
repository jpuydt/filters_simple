(* This file contains the basic definitions and result needed about
   the simple convergence of sequences or series of functions
 *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrnat ssrnum order.
From mathcomp Require Import classical_sets constructive_ereal reals.

From Ici Require Import missing filters.generalities.

Import Order.Theory GRing.Theory Num.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Open Scope ereal_scope.
Open Scope ring_scope.

Section SimpleConvergence_filter.
(* FIXME: find out how to write things... especially, how do I limit (pun intended) myself to
   finite subsets of ℝ?

Variable X: Type.
Definition I := ([set: ℕ] * [set: ℝ])%type.
Definition E := X -> ℝ.

Definition filter_simple_convergence_item:
  fun l: E => (fun i: I => [set f : E | forall s, s ∈ (snd i) -> `|f s - l s| <= 1 / 2 ^+ fst i]).
 *)

  (* RESULT: convergence simple along a filter on X is equivalent to
  convergence at each point to the reals *)
  
End SimpleConvergence_filter.
