(* This file contains the definitions and theorems on asymptotic and local properties of filters *)

From mathcomp Require Import ssreflect ssrbool.
From mathcomp Require Import classical_sets.

From Ici Require Import missing filters.basics filters.generating filters.sequential.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: property local on a filter *)

Definition filter_prop_local {X: Type} (F: Filter X) (P: set X -> Prop) :=
 forall U V, U \is_nb F -> V \is_nb F -> U ⊂ V -> P U -> P V.

Definition filter_prop_locally {X: Type} (F: Filter X) (P: set X -> Prop) :=
  exists U, U \is_nb F /\ P U.

Reserved Notation "[  'locally' F , P ]" (at level 70).
Notation "[ 'locally' F , P ]" := (filter_prop_locally F P).



(* RESULT: FIXME
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_prop_locally_imp
 {X: Type} (F: Filter X) (P Q: set X -> Prop):
 (forall U, P U -> Q U) -> [locally F, P] -> [locally F, Q].
Proof.
move=> PimpQ.
move=> [U [Unb PU]]. (* F4 [locally ...] *)
exists U ; split=> //.
by apply PimpQ.
Qed.



(* DEFINITION: asymptotic property *)

Definition filter_prop_near
  {X: Type} (F: Filter X) (P: X -> Prop) :=
  exists V, V \is_nb F /\ [on V, P].


Reserved Notation "{ 'near' F , P }" (at level 0).
Notation "{ 'near' F , P }" := (filter_prop_near F P).

Lemma filter_prop_near_of_forall
  {X: Type} {F: Filter X} {P: X -> Prop}:
  (forall x, P x) -> {near F, P}.
Proof.
by exists setT.
Qed.

Lemma filter_prop_near_and
  {X: Type} {F: Filter X} {P Q: X -> Prop}:
  {near F, P} -> {near F, Q} -> {near F, (fun x => P x /\ Q x)}.
Proof.
move=> [U [FU PU]]. (* F4 {near ...} *)
move=> [V [FV PV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
split.
- by apply PU.
- by apply PV.
Qed.

Lemma filter_prop_near_imp
  {X: Type} {F: Filter X} {P Q: X -> Prop}:
  {near F, P} -> {near F, (fun x => P x -> Q x)} -> {near F, Q}.
Proof.
move=> [U [Unb PU]]. (* F4 {near ...} *)
move=> [V [Vnb PQV]]. (* F4 {near ...} *)
exists (U ∩ V) ; split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinV].
apply PQV => //.
by apply PU.
Qed.



(* RESULT: the notion of limit is asymptotic
   MC-A: TODO
   mathlib: TODO
 *)

Lemma filter_tends_to_asymptotic
 {X Y: Type} (f g: X -> Y) (F: Filter X) (G: Filter Y):
 {near F, (fun x => f x = g x)}
 -> f @ F --> G
 -> g @ F --> G.
Proof.
move=> [V [Vnb f_eq_g]]. (* F4 {near ...} *)
move=> f_tends_to.
move=> W Wnb. (* F4 filter_tends_to *)
have: [locally F, fun U => [set f x | x in U] ⊂ W] by apply f_tends_to.
move=> [U [Unb fUW]]. (* F4 [locally ...] *)
exists (U ∩ V); split=> //.
rewrite image_sub.
move=> x [xinU xinV] //=.
rewrite -inE in xinU. (* F3 *)
rewrite -inE in xinV. (* F3 *)
rewrite -inE. (* F3 *)
rewrite -f_eq_g //.
in_superset fUW.
by apply imagePin.
Qed.



Definition filter_prop_near2
  {X Y: Type} (F: Filter X) (G: Filter Y) (P: X -> Y -> Prop) :=
  exists U V, [/\ U \is_nb F, V \is_nb G & [on U, (fun x => [on V, P x])]].


Reserved Notation "{ 'near' F & G , P }" (at level 0).
Notation "{ 'near' F & G , P }" := (filter_prop_near2 F G P).



Lemma filter_prop_near2_of_forall
  {X Y: Type} (F: Filter X) (G: Filter Y) (P: X -> Y -> Prop):
  (forall x y, P x y) -> {near F & G, P}.
Proof.
by exists setT; exists setT.
Qed.

Lemma filter_prop_near2_and
  {X Y: Type} (F: Filter X) (G: Filter Y) (P Q: X -> Y -> Prop):
  {near F & G, P}
  -> {near F & G, Q}
  -> {near F & G, fun x y => P x y /\ Q x y}.
Proof.
move=> [U [V [Unb Vnb PUV]]]. (* F4 {near & ... } *)
move=> [A [B [Anb Bnb QAB]]]. (* F4 {near & ... } *)
exists (U ∩ A).
exists (V ∩ B).
split=> //.
move=> x.
rewrite in_setI => /andP [xinU xinA].
move=> y.
rewrite in_setI => /andP [yinV xinB].
split.
- by apply PUV.
- by apply QAB.
Qed.



(* RESULT: a local property is true for all neighbourhoods iff it's true for generating items *)

Lemma filter_from_sequential_family_local_prop
  {X: Type} (G: FilterSequentialFamily X) (P: set X -> Prop):
  filter_prop_local (filter_from_sequential_family G) P -> (forall n, P (seq_item G n))
  <-> (forall  W, W \is_nb (filter_from_sequential_family G) -> P W).
Proof.
move=> Plocal.
split.
- move=> Pitems W Wnb.
  have:= Wnb ; move=> [n GnW]. (* FIXME: decompose Wnb but not lose it? *)
  apply Plocal with (seq_item G n) => //.
- move=> Pallnb n.
  by apply Pallnb.
Qed.



(* RESULT: generating items can simplify existence goals *)

Lemma filter_from_sequential_family_exists
  {X: Type} {G: FilterSequentialFamily X} {P: set X -> Prop}:
  (exists n, P (seq_item G n)) -> [locally (filter_from_sequential_family G), P].
Proof.
move=> [n PGn].
exists (seq_item G n).
by split.
Qed.



(* RESULT: generating items can simplify existence goals *)

Lemma filter_from_sequential_family_exists2
  {X1: Type} {G1: FilterSequentialFamily X1}
  {X2: Type} {G2: FilterSequentialFamily X2}
  {P: set X1 -> set X2 -> Prop}:
  (exists m n, P (seq_item G1 m) (seq_item G2 n))
  -> exists U V, [/\ U \is_nb (filter_from_sequential_family G1),
                     V \is_nb (filter_from_sequential_family G2)
                   & P U V].
Proof.
move=> [m [n PG1mn]].
exists (seq_item G1 m).
exists (seq_item G2 n).
by split.
Qed.



(* RESULT: generating items are representative of general neighbourhoods *)

Lemma filter_from_sequential_family_near
  {X: Type} {G: FilterSequentialFamily X} {P: X -> Prop}:
  (exists n, [on seq_item G n, P]) <-> {near (filter_from_sequential_family G), P}.
Proof.
split.
- move=> [n Hn].
  exists (seq_item G n).
  by split.
- move=> [W [Wnb PW]].
  have:= Wnb; move=> [n Hn]. (* FIXME: decompose Wnb but not lose it? *)
  exists n.
  move=> x xinGn.
  apply PW.
  by apply (in_superset Hn).
Qed.



(* RESULT: a local property is true for all neighbourhoods iff it's true for generating items *)

Lemma filter_from_generating_family_local_prop
  {X: Type} {I: Type} (G: FilterGeneratingFamily X I) (P: set X -> Prop):
  filter_prop_local (filter_from_generating_family G) P -> (forall n, P (gen_item G n))
  <-> (forall  W, W \is_nb (filter_from_generating_family G) -> P W).
Proof.
move=> Plocal.
split.
- move=> Pitems W Wnb.
  have:= Wnb ; move=> [n GnW]. (* FIXME: decompose Wnb but not lose it? *)
  apply Plocal with (gen_item G n) => //.
- move=> Pallnb n.
  by apply Pallnb.
Qed.



(* RESULT: generating items can simplify existence goals *)

Lemma filter_from_generating_family_exists
  {X: Type} {I: Type} {G: FilterGeneratingFamily X I} {P: set X -> Prop}:
  (exists n, P (gen_item G n)) -> [locally (filter_from_generating_family G), P].
Proof.
move=> [n PGn].
exists (gen_item G n).
by split.
Qed.



(* RESULT: generating items can simplify existence goals *)

Lemma filter_from_generating_family_exists2
  {X1: Type} {I1: Type} {G1: FilterGeneratingFamily X1 I1}
  {X2: Type} {I2: Type} {G2: FilterGeneratingFamily X2 I2}
  {P: set X1 -> set X2 -> Prop}:
  (exists m n, P (gen_item G1 m) (gen_item G2 n))
  -> exists U V, [/\ U \is_nb (filter_from_generating_family G1),
                     V \is_nb (filter_from_generating_family G2)
                   & P U V].
Proof.
move=> [m [n PG1mn]].
exists (gen_item G1 m).
exists (gen_item G2 n).
by split.
Qed.



(* RESULT: generating items are representative of general neighbourhoods *)

Lemma filter_from_generating_family_near
  {X: Type} {I: Type} {G: FilterGeneratingFamily X I} {P: X -> Prop}:
  (exists n, [on gen_item G n, P]) <-> {near (filter_from_generating_family G), P}.
Proof.
split.
- move=> [n Hn].
  exists (gen_item G n).
  by split.
- move=> [W [Wnb PW]].
  have:= Wnb; move=> [n Hn]. (* FIXME: decompose Wnb but not lose it? *)
  exists n.
  move=> x xinGn.
  apply PW.
  by apply (in_superset Hn).
Qed.
