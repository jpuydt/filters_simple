(* This file contains the results specific to filters related to natural numbers *)

From mathcomp Require Import ssreflect ssrbool ssrnat ssrfun.
From mathcomp Require Import classical_sets boolp.

From Ici Require Import missing filters.generalities.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: ∞, the filter of infinity in the natural numbers
   MC-A: eventually in topology.v
   mathlib: at_top : filter \nat in order/filter/bases.lean
 *)

Section InftyN.

Definition filter_nat_item (n: ℕ): set ℕ :=  [set m | n <= m].

Lemma filter_nat_item_nonincr:
  forall m n, m <= n
              -> filter_nat_item n ⊂ filter_nat_item m.
Proof.
move=> m n mleqn.
move=> p.
rewrite /filter_nat_item //=. (* F1 *)
by apply leq_trans.
Qed.

Definition filter_nat_sequential := mkFilterSequentialFamily filter_nat_item_nonincr.
Definition filter_nat := filter_from_sequential_family filter_nat_sequential.



Lemma filter_nat_contains_items: forall n, filter_nat_item n \is_nb filter_nat.
Proof.
move=> n.
by exists n.
Qed.



Lemma filter_nat_nontrivial: filter_nontrivial filter_nat.
Proof.
apply filter_sequential_nontrivial.
move=> N.
exists N.
by rewrite //= /filter_nat_item //=. (* F1 *)
Qed.

End InftyN.

Arguments filter_nat_item /.

Notation "∞" := filter_nat.
#[global] Hint Resolve filter_nat_contains_items: core.



(* RESULT: subsequence of a sequence has the same limit
   MC-A: ncvg_sub in altreals/realseq.v?
   mathlib: TODO
 *)
Lemma seq_subseq_tends_to
  {X: Set} {u: ℕ -> X} {F: Filter X}
  {phi: ℕ -> ℕ} {phi_incr: forall m n, m < n -> phi m < phi n}
  :
  u @ ∞ --> F
  -> (u \o phi) @ ∞ --> F.
Proof.
move=> utendsto.
apply filter_tends_to_composition with ∞ => //.
have phi_ge_id: forall n, n <= phi n.
  elim=> [ // | n IHn].
  apply leq_ltn_trans with (phi n) => //.
  by apply phi_incr.
rewrite -filter_tendsto_sequential. (* F1 *)
move=> n.
exists n => //=.
rewrite image_sub /preimage. (* F3? *)
move=> m //= nlem.
by apply leq_trans with m.
Qed.



(* RESULT: the notion of sequence limit is true by shifting
   MC-A: cvg_comp_shift in normedtype.v?
   mathlib: TODO
 *)
Lemma seq_tends_to_shift
 {X: Set} (u: ℕ -> X) (F: Filter X):
 forall N, u @ ∞ --> F <-> (fun n => u (N + n)) @ ∞ --> F.
Proof.
move=> N.
split.
- (* first direction is just subsequencing *)
  have phi_incr: forall m n, m < n -> N + m < N + n.
    move=> m n mltn.
    by rewrite ltn_add2l.
  by apply (seq_subseq_tends_to (phi_incr := phi_incr)).
- (* second direction is more subtle *)
  move=> u_shift_tends.
  rewrite -filter_tendsto_on_sequential => //=. (* F1 *)
  move=> W Wnb.
  move: (u_shift_tends W Wnb) => [V [Vnb uNVW]].
  move: Vnb => [n //= nleqinV].
  exists (N + n).
  apply: subset_trans uNVW.
  move=> y [m Nnlem y_def].
  rewrite -y_def.
  apply (image_subset nleqinV).
  move: Nnlem => /= Nnlem. (* F4?  *)
  move: (nat_add_leq_is_leq_add Nnlem) => [q [nleqq misNq]].
  exists q => //.
  by rewrite misNq.
Qed.



(* RESULT: sequential characterization of limits
   This works only for sequential filters, but since many are,
   this means this result has a high yield.
    MC-A: ???
    mathlib: tendsto_of_subseq_tendsto in order/filter/at_top_bot.lean?
 *)

Section SequentialCharacterization.

Variable X Y: Set.
Variable f: X -> Y.
Variable B: FilterSequentialFamily X.
Variable G: Filter Y.

Lemma sequential_characterisation:
  f @ (filter_from_sequential_family B) --> G
  <-> forall (u: ℕ -> X), u @ ∞ --> (filter_from_sequential_family B)
                          -> (f \o u) @ ∞ --> G.
Proof.
(* one direction is just composition *)
split.
- move=> fFG u uF.
  by apply filter_tends_to_composition with (filter_from_sequential_family B).

(* the reverse direction is the hard part, usually done by contraposition *)
- apply contraPP.
  (* FIXME: moving negations in complex sentences is way too difficult! *)
  rewrite -!existsNP.
  under eq_exists do [rewrite not_implyE].
  move=> [V [Vnb HV]].
  under eq_exists do rewrite not_implyE. (* making the goal look good *)
  (* HV doesn't look good and all I manage to do to improve it is: *)
  do [rewrite -forallNP] in HV.

  (* now we turn exV into the construction of a contradictory sequence *)

  (* fist we get for each integer n a suitable element vn *)
    have exists_vn: forall n, exists (vn: X), [/\ vn ∈ seq_item B n & ~ f vn ∈ V].
    move=> n.
    move: (HV (seq_item B n)).
    rewrite -implypN.
    (* FIXME: ridiculous! *)
    move=> foo.
    move: (foo (filter_sequential_family_contains_items B n)).
    (* FIXME: ridiculous! *)
    move=> bar.
    move: (nonsubset bar) => [y [yisav ynotinV]]. (* F4 *)
    move: yisav => [x xinBn yfx].
    exists x.
    split ; first by rewrite inE.
    rewrite yfx.
    by rewrite inE.

  clear HV. (* we have extracted what we needed from it *)

  (* now we turn the n => vn into a single v: n => vn using some kind of AC *)
  have exists_v: exists (v: ℕ -> X), forall n, [/\ v n ∈ seq_item B n & ~ f (v n) ∈ V].
    pose raw := fun n => constructive_indefinite_description (exists_vn n).
    exists (fun n => proj1_sig (raw n)).
    move=> n.
    by apply: proj2_sig (raw n).

  clear exists_vn.

  (* ok ; now we have the v sequence with nice properties
     let's prove that they mean other properties and get a contradiction
   *)
  move: exists_v => [v /all_and2 [v_inB fv_notin_W]].
  exists v ; split.

  + apply filter_tendsto_to_sequential. (* F1 *)
    move=> n.
    exists (seq_item filter_nat_sequential n). (* F1 *)
    split => //.
    move=> vx [x n_le_x vx_def]. (* F4 *)
    rewrite -vx_def.
    rewrite -inE. (* F3 *)
    have BxBn: seq_item B x ⊂ seq_item B n by apply seq_item_nonincr.
    by in_superset BxBn.

  + rewrite -existsNP.
    exists V.
    rewrite not_implyP.
    split => //.
    move=> [U [Unb fvUV]]. (* F4 *)
    have: U !=set0 by apply filter_nat_nontrivial.
    move=> [x xinU]. (* F4 getting an element in an empty set *)
    rewrite -inE in xinU. (* F3 *)
    have: (f \o v) x ∈ V.
      in_superset fvUV.
      by apply imagePin.
    by apply fv_notin_W.
Qed.


End SequentialCharacterization.
