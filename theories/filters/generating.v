(* This file contains what is needed to build more complex filters - second year material *)


From mathcomp Require Import ssreflect ssrbool ssrnat.
From mathcomp Require Import classical_sets.

From Ici Require Import missing filters.basics.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(* DEFINITION: generating family *)

Record FilterGeneratingFamily (E: Type) (I: Type)
  := mkFilterGeneratingFamily {
  gen_item: I -> set E;
  gen_item_cap: forall i j, exists k, gen_item k ⊂ (gen_item i ∩ gen_item j);
  gen_index_nonempty: [set: I] !=set0;
}.


Section FilterFromGeneratingFamily.

Variable E: Type.
Variable I: Type.
Variable G: FilterGeneratingFamily E I.

Definition filter_from_generating_family_filter: set (set E) := fun A => exists i, gen_item G i ⊂ A.

Lemma filter_from_generating_family_fullset: filter_from_generating_family_filter setT.
Proof.
  move: (gen_index_nonempty G) => [i].
  by exists i.
Qed.

Lemma filter_from_generating_family_nondecr:
  forall C D, C ⊂ D -> filter_from_generating_family_filter C
              -> filter_from_generating_family_filter D.
Proof.
  move=> C D CsubD [m GmC].
  exists m.
  by apply (subset_trans GmC).
Qed.

Lemma filter_from_generating_family_cap:
  forall C D, filter_from_generating_family_filter C -> filter_from_generating_family_filter D
              -> filter_from_generating_family_filter (C ∩ D).
Proof.
  move=> C D [m GmC] [n GnD].
  move: (gen_item_cap G m n) => [p GpCD].
  exists p.
  apply (subset_trans GpCD).
  by apply setISS.
Qed.

Definition filter_from_generating_family :=
  mkFilter
    filter_from_generating_family_fullset
    filter_from_generating_family_nondecr
    filter_from_generating_family_cap.

End FilterFromGeneratingFamily.



(* RESULT: the items used to generate a filter are part of the filter *)

Lemma filter_from_generating_family_contains_items
  {E: Type} {I: Type} {G: FilterGeneratingFamily E I}:
  forall i, gen_item G i \is_nb filter_from_generating_family G.
Proof.
  move=> i.
  by exists i.
Qed.

#[global] Hint Resolve filter_from_generating_family_contains_items: core.



(* RESULT: if the family doesn't contain the empty set, the filter is non trivial *)

Lemma filter_sequential_nontrivial
  {E: Type} {I: Type} {G: FilterGeneratingFamily E I}:
  (forall n, gen_item G n !=set0) -> filter_nontrivial (filter_from_generating_family G).
Proof.
  move=> Gnonempty.
  move=> U [N GNU].
  apply (subset_nonempty GNU).
  by apply Gnonempty.
Qed.
