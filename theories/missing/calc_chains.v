(* those are tactics to work on calculus chains *)
(* this comes from bribe_20230202 *)

From mathcomp Require Import ssreflect order.

Import Order.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Tactic Notation "calc_on" constr(a) := have: (a = a) by [].

Tactic Notation "calc_eq" constr(c) :=
match goal with
| |- @eq _ ?a ?b -> _ =>
  let H := fresh "eq_a_b" in
  let H0 := fresh "eq_b_c" in
  move=> H;
  have H0: b = c; [ | rewrite ?[RHS]H0 in H; move: {H0} H]
| |- is_true (@Order.le _ _ ?a ?b) -> _ =>
  let H := fresh "le_a_b" in
  let H0 := fresh "eq_b_c" in
  move=> H;
  have H0: b = c; [ | rewrite [leRHS]H0 in H; move: {H0} H]
| |- is_true (@Order.lt _ _ ?a ?b) -> _ =>
  let H := fresh "lt_a_b" in
  let H0 := fresh "eq_b_c" in
  move=> H;
  have H0: b = c; [ | rewrite [ltRHS]H0 in H; move: {H0} H]
| _ => fail 1 "Not applicable"
end.

Tactic Notation "calc_le" constr(c) :=
match goal with
| |- @eq _ ?a ?b -> _ =>
  let H := fresh "eq_a_b" in
  let H0 := fresh "le_b_c" in
  move=> H;
  have H0: b <= c; [ | rewrite -?H in H0; move: {H} H0]
| |- is_true (@Order.le _ _ ?a ?b) -> _ =>
  let H := fresh "le_a_b" in
  let H0 := fresh "le_b_c" in
  move=> H;
  have H0: b <= c; [ | move: {H0 H} (le_trans H H0) ]
| |- is_true (@Order.lt _ _ ?a ?b) -> _ =>
  let H := fresh "lt_a_b" in
  let H0 := fresh "le_b_c" in
  move=> H;
  have H0: b <= c; [ | move: {H0 H} (lt_le_trans H H0)]
(*| _ => fail 1 "Not applicable"*)
end.

Tactic Notation "calc_lt" constr(c) :=
match goal with
| |- @eq _ ?a ?b -> _ =>
  let H := fresh "eq_a_b" in
  let H0 := fresh "lt_b_c" in
  move=> H;
  have H0: b < c; [ | rewrite -?H in H0; move: {H} H0]
| |- is_true (@Order.le _ _ ?a ?b) -> _ =>
  let H := fresh "le_a_b" in
  let H0 := fresh "lt_b_c" in
  move=> H;
  have H0: b < c; [ | move: {H0 H} (le_lt_trans H H0) ]
| |- is_true (@Order.lt _ _ ?a ?b) -> _ =>
  let H := fresh "lt_a_b" in
  let H0 := fresh "lt_b_c" in
  move=> H;
  have H0: b < c; [ | move: {H0 H} (lt_trans H H0)]
| _ => fail 1 "Not applicable"
end.
