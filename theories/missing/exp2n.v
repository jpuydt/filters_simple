From mathcomp Require Import ssreflect eqtype ssralg ssrbool ssrnat ssrnum order archimedean.
From mathcomp Require Import reals.
From Ici Require Import missing.notations.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Lemma n_lt_exp2n: forall n, n < 2 ^ n.
Proof.
elim=> [ // | n IHn].
rewrite expnS mul2n -addnn -addn1 //.
apply leq_add => //.
by apply expn_gt0.
Qed.


Open Scope ring_scope.

Lemma exp2n_neq0: forall n, (2: ℝ) ^+ n != 0.
Proof.
move=> n.
apply expf_neq0.
by rewrite lt0r_neq0.
Qed.


Lemma exp2n_incr: forall m n, (m < n)%N -> (2 ^+ m < 2 ^+ n :> ℝ).
Proof.
move=> m n mltn.
rewrite -!natrX ltr_nat.
by rewrite ltn_exp2l.
Qed.



Lemma exp2n_nondecr: forall m n, (m <= n)%N -> (2 ^+ m <= 2 ^+ n :> ℝ).
Proof.
move=> m n mlen.
rewrite -!natrX ler_nat.
by rewrite leq_exp2l.
Qed.



Lemma exp2n_epsilonesque:
  forall ϵ : ℝ, 0 < ϵ -> exists n, 1 / 2 ^+ n < ϵ.
Proof.
move=> ϵ ϵ_gt0.
pose q := Num.bound (ϵ ^-1).
exists q.
have q_gt0: 0 < q%:R :> ℝ.
  apply lt_trans with (ϵ ^-1) => // ; first by rewrite invr_gt0.
  rewrite /q archi_boundP //.
  by rewrite ltW // invr_gt0.
apply lt_trans with (1 / q%:R).
- rewrite ltr_pdivrMr ; last by rewrite exprn_gt0.
  rewrite mulrC.
  rewrite -ltr_pdivrMr.
  + rewrite mul1r invrK mul1r.
    rewrite -natrX ltr_nat.
    by apply n_lt_exp2n.
  + by rewrite mul1r invr_gt0.
- rewrite ltr_pdivrMr //.
  rewrite mulrC -ltr_pdivrMr //.
  rewrite mul1r /q archi_boundP //.
  by rewrite ltW // invr_gt0.
Qed.

Lemma exp2n_gt0: forall n, (2 ^+ n :> ℝ) ∈ [pred t | 0 < t].
Proof.
move=> n.
rewrite inE. (* F3 *)
by apply exprn_gt0.
Qed.
