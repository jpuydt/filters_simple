From mathcomp Require Import ssreflect ssrbool ssrnat order.
From mathcomp Require Import classical_sets.

From Ici Require Import missing.notations.

Import Order.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Open Scope order_scope.


(* this is nondecreasing_seqP in MC-A, but I'd like to avoid using it *)
Lemma seq_nondecreasing_iff_onestep {d: Order.disp_t} {T: porderType d}:
  forall (u: ℕ -> T),
  iff (forall n, u n <= u n.+1) (forall m n, m <= n -> u m <= u n).
Proof.
move=> u.
split.
- move=> onestep m n m_le_n.
  rewrite -(subnKC m_le_n).
  have gen: forall p, u m <= u (m + p).
    elim => [ | p Hp] ; first by rewrite addn0.
    rewrite -addn1 addnA addn1.
    by apply le_trans with (u (m + p)).
  by apply gen.
- move=> gen n.
  by apply gen.
Qed.

(* apparently, the subsets don't have a partially ordered structure *)
Lemma seq_subset_nonincreasing_iff_onestep {E: Type}:
  forall (B: ℕ -> set E),
  iff (forall n, B n.+1 ⊂ B n) (forall m n, m <= n -> B n ⊂ B m).
Proof.
move=> B.
split.
- move=> onestep m n m_le_n.
  rewrite -(subnKC m_le_n).
  have gen: forall p, B (m + p) ⊂ B m.
    elim => [ | p Hp] ; first by rewrite addn0.
    rewrite -addn1 addnA addn1.
    by apply subset_trans with (B (m + p)).
  by apply gen.
- move=> gen n.
  by apply gen.
Qed.
