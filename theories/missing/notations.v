(* this file is about notations that would make sense *)

From mathcomp Require Import ssrbool.
From mathcomp Require Import classical_sets reals.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Reserved Notation "[ \/ P1 , P2 , P3, P4 | P5 ]" (at level 0, format
  "'[hv' [ \/ '['  P1 , '/'  P2 , '/'  P3, '/' P4 ']' '/ '  |  P5 ] ']'").
Inductive or5 (P1 P2 P3 P4 P5 : Prop) : Prop :=
  Or51 of P1 | Or52 of P2 | Or53 of P3 | Or54 of P4 | Or55 of P5.
Notation "[ \/ P1 , P2 , P3 , P4 | P5 ]" := (or5 P1 P2 P3 P4 P5) : type_scope.



Definition ℕ := nat.



Parameter ℝ: realType.



Open Scope classical_set_scope.

Reserved Notation "x ∈ X" (at level 70, no associativity).
Notation "x ∈ X" := (x \in X).
Reserved Notation "A ⊂ B" (at level 70, no associativity).
Notation "A ⊂ B" := (A `<=` B).
Reserved Notation "A ∪ B" (at level 70, no associativity).
Notation "A ∪ B" := (A `|` B).
Reserved Notation "A ∩ B" (at level 70, no associativity).
Notation "A ∩ B" := (A `&` B).



Reserved Notation "[ 'on' X , P ]" (at level 0).
Notation "[ 'on' X , P ]" := (forall x, x ∈ X -> P x).
