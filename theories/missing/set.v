(* This file contains things I find missing for sets in Coq/MC/MC-A *)

From mathcomp Require Import ssreflect ssrbool.
From mathcomp Require Import classical_sets functions.

From Ici Require Import missing.notations.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(* from A ⊂ B, move=> x should give x ∈ A -> x ∈ B instead of A x -> B x *)
Lemma in_superset {T} {A B: set T} {x: T}: A ⊂ B -> x ∈ A -> x ∈ B.
Proof.
move=> sub.
rewrite !in_setE.
by apply sub.
Qed.
Ltac in_superset sub := apply (in_superset sub).


Lemma set1_subset {X: Type} {A: set X} {a: X}: [set a] ⊂ A <-> a ∈ A.
Proof.
split.
- move=> setaA.
  in_superset setaA.
  by rewrite /set1 inE. (* F3 *)
- move=> Aa.
  move=> x.
  rewrite /set1 //=.
  move ->.
  by rewrite -inE.
Qed.



Lemma imagePin {S T: Type} {f: S -> T} {A: set S} {a: S}: a ∈ A -> f a ∈ [set f s | s in A].
Proof.
rewrite !inE.
by apply imageP.
Qed.

Lemma image_cst {S: Type} {T: Type} (a: T): range (@cst S T a) ⊂ [set a].
Proof.
move=> x.
rewrite //= /cst.
by move=> [s _ ->].
Qed.
