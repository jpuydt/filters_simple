(* This file is about bounded real-valued functions *)

From mathcomp Require Import ssreflect ssralg ssrnum ssrbool ssrfun order.
From mathcomp Require Import boolp classical_sets reals.

From Ici Require Import missing.notations missing.set.

Import Order.Theory Num.Theory GRing.Theory.
Import functions.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Open Scope ring_scope.


(* DEFINITION: bounded real function on a domain *)
(* FIXME: there is a notion of bounded function... in MC-A's normedtype.v -- too abstract! *)

Definition bounded {X: Type} (f: X -> ℝ) (A: set X) := exists M, forall x, x ∈ A -> `|f x| <= M.



(* RESULT: boundedness implies upper-bounded (FIXME: depends on too complex things) *)

Lemma bounded_has_ubound {X: Type} (f: X -> ℝ) (A: set X): bounded f A -> has_ubound [set f x | x in A].
Proof.
move=> [M MboundfA].
have range_f_itv: [set f x | x in A] ⊂ [set y | -M <= y <= M].
  rewrite image_sub /preimage.
  move=> a Aa //=.
  rewrite -inE in Aa. (* F3 *)
  rewrite -ler_norml.
  by apply MboundfA.

apply subset_has_ubound with [set y | -M <= y <= M] => //.
rewrite -set_interval.set_itvcc.
apply set_interval.has_ubound_itv.
Qed.



(* RESULT: boundedness is nonincreasing on the domain
   MC-A: TODO
   mathlib: TODO
 *)

Lemma bounded_on_nonincr
  {X: Type} (A B: set X) (f: X -> ℝ):
  A ⊂ B -> bounded f B -> bounded f A.
Proof.
move=> AsubB.
move=> [M fBM]. (* F4 bounded *)
exists M.
move=> x Ax.
apply fBM.
by in_superset AsubB.
Qed.



(* RESULT: a function is bounded if and only if it's controlled below and above *)

Lemma bounded_double_inequality
 {X: Type} (A: set X) (f: X -> ℝ):
 (exists a b, forall x, x ∈ A -> a <= f x <= b) -> bounded f A.
Proof.
move=> [a [b Hab]].
exists (`|a|+`|b|).
move=> x xinA.
have: a <= f x <= b.
  by apply Hab.
move=> /andP [Ha Hb] {Hab}.
rewrite ler_norml.
apply/andP ; split.
- rewrite opprD lerBDr.
  apply ler_wpDr => //.
  apply le_trans with a => //.
  rewrite -lerN2 opprK -normrN.
  by apply ler_norm.
- apply le_trans with b => //.
  apply ler_wpDl => //.
  by apply ler_norm.
Qed.



(* RESULT: boundedness of a product by a constant *)

Lemma bounded_product_constant
  {X: Type} (A: set X) (f: X -> ℝ):
  forall (C: ℝ), bounded f A -> bounded (C \*o f) A.
Proof.
move=> C [M fAM].
exists (`|C| * M).
move=> x xinA.
rewrite normrM.
apply ler_pM => //.
by apply fAM.
Qed.



(* RESULT: boundedness of a sum *)

Lemma bounded_sum
 {X: Type} (A: set X) (f g: X -> ℝ):
 bounded f A -> bounded g A -> bounded (f \+ g) A.
Proof.
move=> [M fAM].
move=> [N gAN].
exists (M + N).
move=> a xinA.
apply le_trans with (`|f a| + `|g a|).
- by apply ler_normD.
- apply lerD.
  + by apply fAM.
  + by apply gAN.
Qed.



(* RESULT: boundedness by composition *)

Lemma bounded_comp
  {X Y: Type} (A: set X) (B: set Y)
  (f: X -> Y) (g: Y -> ℝ):
  (forall x, x ∈ A -> f x \in B) -> (bounded g B) -> bounded (g \o f) A.
Proof.
move=> fAB [M gMB].
exists M.
move=> x xinA.
apply gMB.
by apply fAB.
Qed.
