From mathcomp Require Import ssreflect ssrbool ssrnat.

From Ici Require Import missing.notations.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(* leq_ltn_trans does exist *)
Lemma ltn_leq_trans n m p : m < n -> n <= p -> m < p.
Proof.
move=> mltn.
by apply leq_trans.
Qed.

Lemma nat_even_odd_dec: forall n, {m | n = 2 * m} + {m | n = 2 * m + 1}.
Proof.
induction n  ; first by (left ; exists 0).
case IHn.
- move=> [m n2m].
  right ; exists m.
  by rewrite -addn1 n2m.
- move=> [m n2m1].
  left ; exists (m + 1).
  rewrite -addn1 n2m1.
  by rewrite mulnDr muln1 -addnA.
Qed.

Lemma nat_add_leq_is_leq_add:
  forall m n p, m + n <= p -> exists q, n <= q /\ p = m + q.
Proof.
move=> m n p mnleqp.
rewrite -(@subnKC (m + n) p) => //.
exists (n + (p - (m + n))).
split ; first by apply leq_addr.
by rewrite -addnA.
Qed.
