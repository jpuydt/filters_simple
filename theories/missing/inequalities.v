(* this file is to get sensible inequalities *)

From mathcomp Require Import ssreflect ssralg ssrbool ssrfun ssrnum order.
From mathcomp Require Import boolp classical_sets reals.

From Ici Require Import missing.notations.

Import Order.Theory Num.Theory GRing.Theory.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Open Scope ring_scope.



(* FIXME: in MC's ssrnum.v, the hypothesis is about an invertible
   positive element... but since positivity implies invertibility,
   (unitf_gt0), this is cumbersome. The following lemmas are more
   direct.
 *)

Lemma my_ler_pV2: {in [pred x | 0 < x] &, {mono (@GRing.inv ℝ): x y /~ x <= y}}.
Proof.
move=> x y.
rewrite !inE. (* F3 *)
move=> x_gt0 y_gt0.
rewrite -(ler_pM2l x_gt0) -(ler_pM2r y_gt0).
by rewrite !(divrr, mulrVK) ?mul1r ?unitf_gt0.
Qed.

Lemma my_ltr_pV2: {in [pred x | 0 < x] &, {mono (@GRing.inv ℝ): x y /~ x < y}}.
Proof.
exact: leW_nmono_in my_ler_pV2.
Qed.
